#pragma once
#include <string>
#include <vector>
#include <cstdint>
#include <memory>
#include <map>
#include "Question.h"

#define REQ_NAME "username"
#define REQ_PASS "password"
#define REQ_SIGNUP_EMAIL "email"

class IRequestHandler;

using std::string;

typedef std::vector<uint8_t> buffer;

typedef std::pair<std::string, double> highscore;



typedef string LoggedUser;

struct RoomData
{
	unsigned int id;
	string name;
	int maxPlayers; //If we ever want to store data as a negative (say, -1 for unlimited)
	int questionCount;
	int timePerQuestion;
	int isActive;
};

struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;
};

struct LoginRequest
{
	string username;
	string password;
};

struct SignupRequest
{
	string username;
	string password;
	string email;
};

struct Request
{
	uint16_t id = 0;
	time_t receivalTime = 0;
	buffer buffer;
};

struct RequestResult
{
	buffer response;
	std::unique_ptr<IRequestHandler> newHandler = nullptr;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	string roomName;
	int maxUsers;
	int questionCount;
	int answerTimeOut;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
};

//Responses

struct GetRoomStateResponse
{
	int gameStatus;
	std::vector<string> players;
	int questionCount;
	int answerTimeOut;
};

struct ServerErrorResponse
{
	string message;
	//Optional
	int status = 0;
};

struct UserErrorResponse
{
	string message;
	//Optional
	int status = 0;
};

struct GetRoomsResponse
{
	std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
	std::vector<string> names;
};

struct HighscoreResponse
{
	std::vector<highscore> highscores;
};

struct GetQuestionResponse
{
	unsigned int status;
	string question;
	std::map<unsigned int, string> answers;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct PlayerResults
{
	string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
};

struct SignupResponse {};

struct LoginResponse {};

struct LogoutResponse {};

struct JoinRoomResponse {};

struct LeaveRoomResponse {};

struct CreateRoomResponse {};

struct CloseRoomResponse {};

struct StartGameResponse {};

struct LeaveGameResponse{};