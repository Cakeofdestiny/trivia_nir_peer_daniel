#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(buffer bytes)
{
	json j_from_string = json::parse(bytes);
	LoginRequest login{
		j_from_string[ReqFields::USERNAME].get<std::string>(),
		j_from_string[ReqFields::PASSWORD].get<std::string>()
	};
	return login;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(buffer bytes)
{
	json j_from_string = json::parse(bytes);
	SignupRequest signup{
		j_from_string[ReqFields::USERNAME].get<std::string>(),
		j_from_string[ReqFields::PASSWORD].get<std::string>(),
		j_from_string[ReqFields::EMAIL].get<std::string>()
	};
	return signup;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(buffer bytes)
{
	json j_from_string = json::parse(bytes);
	GetPlayersInRoomRequest playersInRoom{
		j_from_string[ReqFields::ROOM_ID].get<unsigned int>()
	};
	return playersInRoom;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(buffer bytes)
{
	json j_from_string = json::parse(bytes);
	JoinRoomRequest joinRoom{
		j_from_string[ReqFields::ROOM_ID].get<unsigned int>()
	};
	return joinRoom;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(buffer bytes)
{
	json j_from_string = json::parse(bytes);
	CreateRoomRequest createRoom{
		j_from_string[ReqFields::ROOM_NAME].get<std::string>(),
		j_from_string[ReqFields::MAX_USERS].get<int>(),
		j_from_string[ReqFields::QUESTIONS].get<int>(),
		j_from_string[ReqFields::TIMEOUT].get<int>()
	};
	return createRoom;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(buffer bytes)
{
	json j_from_bson = json::from_bson(bytes);
	SubmitAnswerRequest submitAnswer{
		j_from_bson[ReqFields::ANS_ID].get<int>()
	};
	return submitAnswer;
}
