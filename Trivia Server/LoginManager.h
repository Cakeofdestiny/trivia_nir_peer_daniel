#pragma once

#include "IDatabase.h"
#include "exceptions.h"
#include "Constants.h"
#include <algorithm>
#include <vector>
#include <mutex>
#include <string>

class LoginManager
{
public:
	explicit LoginManager(IDatabase& database);
	~LoginManager();

	void signup(string email, string username, string password);
	bool login(string username, string password);
	void logout(string username);

	//In order to prevent unintended copying
	LoginManager(const LoginManager&) = delete;
	LoginManager(LoginManager&&) = delete;
	LoginManager& operator=(const LoginManager&) = delete;
	LoginManager& operator=(LoginManager&&) = delete;

private:
	std::vector<LoggedUser> _loggedUsers;
	std::mutex _dataMutex;
	bool _checkIfLogged(string username);
	IDatabase& _database;
};

