#include "JsonResponsePacketSerializer.h"

buffer JsonResponsePacketSerializer::serializeOK()
{
	//Calculate message size
	string messageHeader = Helper::getPaddedNumber(0, SIZE_BYTES_LEN) + std::to_string(RESP_OK);
	//Add the message and resp code (OK) and return
	buffer respBuffer(messageHeader.c_str(), messageHeader.c_str() + messageHeader.size());
	return respBuffer;
}

buffer JsonResponsePacketSerializer::serializeResponse(UserErrorResponse resp)
{
	json j;
	j[RespFields::ERR] = resp.message;
	j[RespFields::STATUS] = resp.status;
	return JsonResponsePacketSerializer::_createBuffer(j, RESP_USER_ERROR);
}

buffer JsonResponsePacketSerializer::serializeResponse(ServerErrorResponse resp)
{
	json j;
	j[RespFields::ERR] = resp.message;
	j[RespFields::STATUS] = resp.status;
	return JsonResponsePacketSerializer::_createBuffer(j, RESP_SERVER_ERROR);
}

buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse resp)
{
	json j;
	j[RespFields::ROOMS] = {};
	for (auto iter = resp.rooms.begin(); iter != resp.rooms.end(); ++iter)
	{
		j[RespFields::ROOMS].push_back({
			{RespFields::ROOM_ID, std::to_string(iter->id)},
			{RespFields::ROOM_ACTIVE, std::to_string(iter->isActive)},
			{RespFields::ROOM_MAX_PLAYERS, std::to_string(iter->maxPlayers)},
			{RespFields::ROOM_NAME, iter->name },
			{RespFields::ROOM_QUESTION_COUNT, iter->questionCount},
			{RespFields::ROOM_TIME_PER_Q, std::to_string(iter->timePerQuestion)}
			});
	}
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse resp)
{
	json j;
	j[RespFields::NAMES] = resp.names;
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(HighscoreResponse resp)
{
	json j;
	j[RespFields::HIGHSCORES] = {};
	for (auto iter = resp.highscores.begin(); iter != resp.highscores.end(); ++iter)
	{
		j[RespFields::HIGHSCORES].push_back({
			{RespFields::HIGHSCORE_USER, iter->first},
			{RespFields::HIGHSCORE_SCORE, std::to_string(iter->second) }
			});
	}
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse resp)
{
	json j;
	j[RespFields::GAME_STATUS] = std::to_string(resp.gameStatus);
	j[RespFields::PLAYERS] = resp.players;
	j[RespFields::QUESTION_COUNT] = std::to_string(resp.questionCount);
	j[RespFields::TIMEOUT] = std::to_string(resp.answerTimeOut);
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse resp)
{
	return serializeOK();
}

buffer JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse resp)
{
	json j;
	j[RespFields::STATUS] = std::to_string(resp.status);
	j[RespFields::QUESTION] = resp.question;
	j[RespFields::ANSWERS] = resp.answers;
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse resp)
{
	json j;
	j[RespFields::STATUS] = std::to_string(resp.status);
	j[RespFields::ANS_ID] = std::to_string(resp.correctAnswerId);
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse resp)
{
	json j;
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::serializeResultsResponse(GetGameResultsResponse resp)
{
	json j;
	j[RespFields::STATUS] = std::to_string(resp.status);
	j[RespFields::RESULTS] = {};
	for (auto iter = resp.results.begin(); iter != resp.results.end(); ++iter)
	{
		j[RespFields::RESULTS].push_back({
			{RespFields::RESULTS_USERNAME, iter->username},
			{RespFields::RESULTS_CORRECT_ANSWER_COUNT, std::to_string(iter->correctAnswerCount)},
			{RespFields::RESULTS_WRONG_ANSWER_COUNT, std::to_string(iter->wrongAnswerCount)},
			{RespFields::RESULTS_AVERAGE_ANSWER_TIME, std::to_string(iter->averageAnswerTime)}
			});
	}
	return JsonResponsePacketSerializer::_createBuffer(j);
}

buffer JsonResponsePacketSerializer::_createBuffer(json& responseJson, unsigned int code)
{
	string jsonString = responseJson.dump();
	//Calculate message size
	string messageHeader = Helper::getPaddedNumber(jsonString.size(), SIZE_BYTES_LEN) + std::to_string(code);
	//First add the message size and resp code
	buffer respBuffer(messageHeader.c_str(), messageHeader.c_str() + messageHeader.size());
	//Then concatenate the actual jsonbuffer
	respBuffer.insert(respBuffer.end(), jsonString.begin(), jsonString.end());

	return respBuffer;
}
