#pragma once
#include <string>
#include <vector>
#include "IDatabase.h"
#include "Structs.h"

using std::string;

class HighscoreTable
{
public:
	explicit HighscoreTable(IDatabase& database);
	~HighscoreTable();
	
	//Return a vector of high scores
	std::vector<highscore> getHighscores();
	//Register a new score
	void registerScore(highscore score);

	//In order to prevent unintended copying
	HighscoreTable(const HighscoreTable&) = delete;
	HighscoreTable(HighscoreTable&&) = delete;
	HighscoreTable& operator=(const HighscoreTable&) = delete;
	HighscoreTable& operator=(HighscoreTable&&) = delete;

private:
	IDatabase& _database;
};