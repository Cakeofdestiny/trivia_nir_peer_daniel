#include "Room.h"

Room::Room(unsigned int id, string name, int maxPlayers, int questionCount, int timePerQuestion, int isActive)
{
	this->_metadata.id = id;
	this->_metadata.maxPlayers = maxPlayers;
	this->_metadata.name = name;
	this->_metadata.questionCount = questionCount;
	this->_metadata.timePerQuestion = timePerQuestion;
	this->_metadata.isActive = isActive;
}

Room::Room(const Room& other)
{
	//Copy Constructor
	this->_metadata = other._metadata;
	this->_users = other._users;
}

void Room::addUser(std::string name)
{
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	if (static_cast<int>(this->_users.size()) < this->_metadata.maxPlayers)
	{
		//case not over max players
		this->_users.push_back(LoggedUser(name));
	}
	else
	{
		//If we exceeded max players
		throw UserError(Errors::EXCEEDED_MAX_USERS);
	}
}

void Room::removeUser(std::string username)
{
	//find the user and erase him
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	this->_users.erase(std::remove(this->_users.begin(), this->_users.end(), username), this->_users.end());
}

std::vector<LoggedUser> Room::getAllUsers()
{
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	return this->_users;
}

void Room::setRoomStatus(int stat)
{
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	this->_metadata.isActive = stat;
}

const RoomData& Room::getRoomData()
{
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	return this->_metadata;
}

unsigned int Room::getRoomID()
{
	std::lock_guard<std::mutex> guard(this->_dataMutex);
	return this->_metadata.id;
}

void Room::setGamePtr(GamePtr game)
{
	this->_game = game;
}
