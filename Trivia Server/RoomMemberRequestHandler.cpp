#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& requestHandler, RoomPtr room) : 
	_user(user), _roomManager(RoomManager::getInstance()), _handlerFactory(requestHandler), _room(room)
{
}

bool RoomMemberRequestHandler::isRequestRelevant(Request req)
{
	return (req.id == REQ_LEAVE_ROOM) || (req.id == REQ_GET_STATE);
}

RequestResult RoomMemberRequestHandler::handleRequest(Request req)
{
	switch (req.id)
	{
		break;
	case REQ_LEAVE_ROOM:
		return leaveRoom(req);
		break;
	case REQ_GET_STATE:
		return getRoomState(req);
		break;
	default:
		throw ServerError(Errors::ID_MISMATCH);
		break;
	}
}

void RoomMemberRequestHandler::terminateSession()
{
	this->_handlerFactory.getLoginManager().logout(this->_user);
	this->_room->removeUser(this->_user);
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request req)
{
	LeaveRoomResponse resp;
	RequestResult leaveResponse;
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	this->_room->removeUser(this->_user);
	leaveResponse.newHandler = this->_handlerFactory.createMenuRequestHandler(this->_user);
	leaveResponse.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return leaveResponse;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request req)
{
	GetRoomStateResponse resp;
	RequestResult result;
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	RoomData currentRoomData = this->_room->getRoomData();
	//get parameters of response
	resp.players = this->_room->getAllUsers();
	resp.gameStatus = currentRoomData.isActive;
	resp.answerTimeOut = currentRoomData.timePerQuestion;
	resp.questionCount = currentRoomData.questionCount;
	result.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return result;
}
