#pragma once

#include "LoginManager.h"
#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "Structs.h"
#include <mutex>

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& LogManager, RequestHandlerFactory& handlerFact);
	~LoginRequestHandler();

	virtual bool isRequestRelevant(Request req);
	virtual RequestResult handleRequest(Request req);

private:
	RequestResult login(Request req);
	RequestResult signup(Request req);

	LoginManager& _loginManager;
	RequestHandlerFactory& _handlerFactory;
	std::mutex _loginManagerMutex;
};
