#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "Structs.h"
#include "IDatabase.h"
#include <vector>
#include <mutex>

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& requestHandler, RoomPtr room);

	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request req);
	virtual void terminateSession();

private:
	RequestResult leaveRoom(Request req);
	RequestResult getRoomState(Request req);

	RoomPtr _room;
	LoggedUser _user;
	RoomManager& _roomManager;
	RequestHandlerFactory& _handlerFactory;
	std::mutex _roomMutex;
};