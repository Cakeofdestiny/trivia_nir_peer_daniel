#pragma once

#include <memory>
#include "LoginManager.h"
#include "IRequestHandler.h"
//DO NOT rearrange these includes or else horrors will occur

#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "HighscoreTable.h"
#include "RoomManager.h"
#include "IDatabase.h"
#include "Game.h"
#include "GameRequestHandler.h"

class RequestHandlerFactory
{
public:
	explicit RequestHandlerFactory(IDatabase& database);

	//In order to prevent unintended copying
	RequestHandlerFactory(const RequestHandlerFactory&) = delete;
	RequestHandlerFactory(RequestHandlerFactory&&) = delete;
	RequestHandlerFactory& operator=(const RequestHandlerFactory&) = delete;
	RequestHandlerFactory& operator=(RequestHandlerFactory&&) = delete;
	LoginManager& getLoginManager();

	std::unique_ptr<IRequestHandler> createLoginRequestHandler();
	std::unique_ptr<IRequestHandler> createMenuRequestHandler(LoggedUser user);
	std::unique_ptr<IRequestHandler> createRoomMemberRequestHandler(LoggedUser user, RoomPtr room);
	std::unique_ptr<IRequestHandler> createRoomAdminRequestHandler(LoggedUser user, RoomPtr room);
	std::unique_ptr<IRequestHandler> createGameRequestHandler(LoggedUser user,GamePtr game);

private:
	LoginManager _loginManager;
	HighscoreTable _highscoreTable;
	IDatabase& _database;
};

