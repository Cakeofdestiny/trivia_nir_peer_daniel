#include "RoomAdminRequestHandler.h"

#define START_GAME 1
#define STOP_GAME -1

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user,RequestHandlerFactory& requestHandler, RoomPtr room) :
	_user(user), _roomManager(RoomManager::getInstance()), _handlerFactory(requestHandler), _room(room)
{
}

bool RoomAdminRequestHandler::isRequestRelevant(Request req)
{
	return((req.id == REQ_CLOSE_ROOM) || (req.id == REQ_START_GAME) || (req.id == REQ_GET_STATE));
}

RequestResult RoomAdminRequestHandler::handleRequest(Request req)
{
	switch (req.id)
	{
		break;
	case REQ_CLOSE_ROOM:
		return closeRoom(req);
		break;
	case REQ_GET_STATE:
		return getRoomState(req);
		break;
	case REQ_START_GAME:
		return startGame(req);
		break;
	default:
		throw ServerError(Errors::ID_MISMATCH);
		break;
	}
}

void RoomAdminRequestHandler::terminateSession()
{
	this->_handlerFactory.getLoginManager().logout(this->_user);
	//Close the room because the manager left.
	this->closeRoom({});
}

RequestResult RoomAdminRequestHandler::closeRoom(Request req)
{
	CloseRoomResponse resp;
	RequestResult result;
	//lock
	std::lock_guard<std::mutex> guard(this->_roomManagerMutex);
	//delete room
	this->_room->removeUser(this->_user);
	this->_roomManager.deleteRoom(this->_room->getRoomData().id);
	this->_room->setRoomStatus(ROOM_CLOSED);
	result.response = JsonResponsePacketSerializer::serializeResponse(resp);
	result.newHandler = this->_handlerFactory.createMenuRequestHandler(this->_user);
	return result;
}

RequestResult RoomAdminRequestHandler::startGame(Request req)
{
	StartGameResponse resp;
	RequestResult result;
	//lock
	std::lock_guard<std::mutex> guard(this->_roomManagerMutex);
	std::vector<Question> vec;
	//start game
	RoomPtr roomStart = this->_roomManager.getRoomByID(this->_room->getRoomData().id);
	this->_game = GamePtr(new Game(vec,this->_room->getRoomData()));
	this->_game->updateUsers(this->_room->getAllUsers());
	roomStart->setGamePtr(this->_game);
	roomStart->setRoomStatus(ROOM_PLAYING);
	result.response = JsonResponsePacketSerializer::serializeResponse(resp);
	//TODO: Change handler to game handler
	result.newHandler = nullptr;
	return result;
}

RequestResult RoomAdminRequestHandler::getRoomState(Request req)
{
	GetRoomStateResponse resp;
	RequestResult result;
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	RoomData currentRoomData = this->_room->getRoomData();
	//get parameters of response
	resp.players = this->_room->getAllUsers();
	resp.gameStatus = currentRoomData.isActive;
	resp.answerTimeOut = currentRoomData.timePerQuestion;
	resp.questionCount = currentRoomData.questionCount;
	result.response = JsonResponsePacketSerializer::serializeResponse(resp);
	return result;
}
