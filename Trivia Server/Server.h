#pragma once
#include "IDatabase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"


//Run the server
class Server
{
public:
	Server(IDatabase& database);
	~Server();

	void run();

private:
	IDatabase& _database;
	Communicator _communicator;
	RequestHandlerFactory _handlerFactory;
};

