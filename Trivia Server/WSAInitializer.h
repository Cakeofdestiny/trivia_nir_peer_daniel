#pragma once

#define WSA_ERR "WSAStartup Failed"
#include <winsock2.h>
#include <windows.h>
#include <exception>
#pragma comment(lib, "ws2_32.lib")

class WSAInitializer
{
public:
	WSAInitializer();
	~WSAInitializer();
};

