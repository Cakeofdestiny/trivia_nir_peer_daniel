#include "HighscoreTable.h"

HighscoreTable::HighscoreTable(IDatabase& database) : _database(database) {}

HighscoreTable::~HighscoreTable()
{
}

std::vector<highscore> HighscoreTable::getHighscores()
{
	return this->_database.getHighscores();
}

void HighscoreTable::registerScore(highscore score)
{
	this->_database.registerHighscore(score);
}
