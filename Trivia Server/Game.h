#pragma once
#include <iostream>
#include "Structs.h"
#include "Question.h"
#include "Constants.h"
#include "exceptions.h"
#include <time.h> 
#include <mutex>
#include <vector>

class Game
{
public:
	Game(std::vector<Question> questions, RoomData roomdata);
	~Game();

	Question& getQuestionForUser(string User);
	AnswerCodes submitAnswer(unsigned int answer,string user);
	void removePlayer(string user);
	unsigned int getCorrectId();
	void updateUsers(std::vector<LoggedUser> users);

private:
	unsigned int getdiffTime();
	void GameOver(string user);
	bool updateQuestion();
	void updateCorrectQuestion(string user);

	std::vector<Question> _questions;
	std::map<LoggedUser, GameData> _players;
	int _questionIndex;
	RoomData _roomData;
	time_t _startQuestionTime;
	bool _askedBefore;
	std::mutex _playersMutex;
	std::mutex _questionsMutex;
	std::mutex _timeMutex;
};