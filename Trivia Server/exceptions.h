#pragma once
#include <exception>

class socketRecvError : public std::exception
{
public:
	socketRecvError(const char* what) : std::exception(what) {}
};

class socketSendError : public std::exception
{
public:
	socketSendError(const char* what) : std::exception(what) {}
};

class ServerError : public std::exception
{
public:
	ServerError(const char* what) : std::exception(what) {}
};

class UserError : public std::exception
{
public:
	UserError(const char* what) : std::exception(what) {}
};