//If you change the order of these, the program WILL NOT compile
#include "MongoDatabase.h"
#include "Server.h"
#include <iostream>

#define OK 0

//Just a random testing function :)
void testScoreRegistration()
{
	MongoDatabase& db = MongoDatabase::getInstance();
	for (size_t i = 0; i < 5; i++)
	{
		highscore score;
		std::cin >> score.first;
		score.second = (double)(std::rand() % 1000) / 13.37;
		db.registerHighscore(score);
	}

	for (auto score : db.getHighscores())
	{
		std::cout << score.first << " : " << score.second << std::endl;
	}
}

int main()
{
	Server server(MongoDatabase::getInstance());
	server.run();

	return OK;
}