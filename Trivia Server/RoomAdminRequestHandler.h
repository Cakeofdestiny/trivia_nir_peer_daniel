#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "Structs.h"
#include "IDatabase.h"
#include <mutex>

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser user, RequestHandlerFactory& requestHandler, RoomPtr room);

	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request req);
	virtual void terminateSession();

private:
	RequestResult closeRoom(Request req);
	RequestResult startGame(Request req);
	RequestResult getRoomState(Request req);

	GamePtr _game;
	RoomPtr _room;
	LoggedUser _user;
	RoomManager& _roomManager;
	RequestHandlerFactory& _handlerFactory;
	std::mutex _roomManagerMutex;
	std::mutex _roomMutex;
};