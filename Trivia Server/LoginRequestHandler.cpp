#include "LoginRequestHandler.h"

#include "RequestHandlerFactory.h"

LoginRequestHandler::LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory& handlerFactory)
	: _loginManager(loginManager), _handlerFactory(handlerFactory)
{
}

LoginRequestHandler::~LoginRequestHandler()
{
}

RequestResult LoginRequestHandler::login(Request req)
{
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(req.buffer);
	RequestResult result;

	//lock
	std::lock_guard<std::mutex> guard(this->_loginManagerMutex);
	//Login succeeded
	if (this->_loginManager.login(loginReq.username, loginReq.password))
	{
		LoginResponse resp;
		result.response = JsonResponsePacketSerializer::serializeResponse(resp);
		//lock
		result.newHandler = this->_handlerFactory.createMenuRequestHandler(loginReq.username);
	}
	else
	{
		throw UserError(Errors::INVALID_LOGIN);
	}

	return result;
}

RequestResult LoginRequestHandler::signup(Request req)
{
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignUpRequest(req.buffer);
	RequestResult result;
	SignupResponse resp;
	//lock
	std::lock_guard<std::mutex> guard(this->_loginManagerMutex);
	this->_loginManager.signup(signupReq.email, signupReq.username, signupReq.password);
	result.response = JsonResponsePacketSerializer::serializeResponse(resp);
	//lock
	result.newHandler = this->_handlerFactory.createMenuRequestHandler(signupReq.username);
	return result;
}


bool LoginRequestHandler::isRequestRelevant(Request req)
{
	//Check if the code of the messege is Log in \ sign in
	return (req.id == REQ_LOGIN) || (req.id == REQ_SIGNUP);
}

RequestResult LoginRequestHandler::handleRequest(Request req)
{
	switch (req.id)
	{
	case REQ_LOGIN:
		return login(req);
		break;
	case REQ_SIGNUP:
		return signup(req);
		break;
	default:
		throw ServerError(Errors::ID_MISMATCH);
		break;
	}
}