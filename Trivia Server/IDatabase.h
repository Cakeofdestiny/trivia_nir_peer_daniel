#pragma once
#include <string>
#include "Structs.h"
#include "Constants.h"

using std::string;

//Database interface
class IDatabase
{
public:
	//User management functions
	virtual bool doesUserExist(string username) = 0;
	virtual bool checkPassword(string username, string password) = 0;
	virtual void signupUser(string email, string username, string password) = 0;
	virtual std::vector<highscore> getHighscores(unsigned int num_scores=DEFAULT_HIGHSCORE_LIMIT) = 0;
	virtual void registerHighscore(highscore score) = 0;
};
