#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(GamePtr game, LoggedUser& user, RequestHandlerFactory& handlerFactory) : 
	_game(game),_user(user),_handlerFactory(handlerFactory)
{

}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(Request req)
{
	return (req.id==REQ_QUESTION) || (req.id==REQ_ANSWER) || (req.id==REQ_RESULTS) || (req.id==REQ_LEAVE);
}

RequestResult GameRequestHandler::handleRequest(Request req)
{
	//if (isRequestRelevant(req))
	//{
		switch (req.id)
		{
		case REQ_QUESTION:
			return getQuestion(req);
			break;
		case REQ_ANSWER:
			return submitAnswer(req);
			break;
		case REQ_RESULTS:
			return getGameResults(req);
			break;
		case REQ_LEAVE:
			return leaveGame(req);
			break;
		default:
			throw ServerError(Errors::ID_MISMATCH);
			break;
		}
	//}
}

RequestResult GameRequestHandler::getQuestion(Request req)
{
	RequestResult res;
	GetQuestionResponse response;
	//lock
	std::lock_guard<std::mutex> guard(this->_gameMutex);
	//Get the question by the order
	Question& ques = this->_game->getQuestionForUser(this->_user);
	//Get all posiblle answers
	response.answers = updatePossibleAnswers(ques.getPossibleAnswers());
	response.question = ques.getQuestion();
	response.status = RESP_OK;
	res.response = JsonResponsePacketSerializer::serializeResponse(response);
	return res;
}

RequestResult GameRequestHandler::submitAnswer(Request req)
{
	RequestResult res;
	SubmitAnswerRequest submitReq = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(req.buffer);
	SubmitAnswerResponse response;
	//lock
	std::lock_guard<std::mutex> guard(this->_gameMutex);
	//Get if it's a right answer or not
	response.status = this->_game->submitAnswer(submitReq.answerId, this->_user);
	//get the right answwer id
	response.correctAnswerId = this->_game->getCorrectId();
	res.response = JsonResponsePacketSerializer::serializeResponse(response);
	return res;
}

RequestResult GameRequestHandler::getGameResults(Request req)
{
	RequestResult res;
	GetGameResultsResponse response;
	res.response = JsonResponsePacketSerializer::serializeResultsResponse(response);
	res.newHandler = nullptr;
	return res;
}

RequestResult GameRequestHandler::leaveGame(Request req)
{
	RequestResult res;
	LeaveGameResponse leaveResp;
	//lock
	std::lock_guard<std::mutex> guard(this->_gameMutex);
	this->_game->removePlayer(this->_user);
	res.response = JsonResponsePacketSerializer::serializeResponse(leaveResp);
	res.newHandler = this->_handlerFactory.createMenuRequestHandler(this->_user);
	return res;
}

std::map<unsigned int, string> GameRequestHandler::updatePossibleAnswers(std::vector<string> answers)
{
	std::map<unsigned int, string> answersMap;
	answersMap[FIRST_ANSWER] = answers[FIRST_ANSWER];
	answersMap[SECOND_ANSWER] = answers[SECOND_ANSWER];
	answersMap[THIRD_ANSWER] = answers[THIRD_ANSWER];
	answersMap[FOURTH_ANSWER] = answers[FOURTH_ANSWER];
	return answersMap;
}
