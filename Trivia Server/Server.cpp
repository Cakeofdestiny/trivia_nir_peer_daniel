#include "Server.h"

Server::Server(IDatabase& database) 
	: _database(database), _handlerFactory(database), _communicator(_handlerFactory) {}

Server::~Server()
{
}

void Server::run()
{
	this->_communicator.bindAndListen();
}
