#include "MongoDatabase.h"

mongocxx::database& MongoDatabase::_getDb()
{
	thread_local mongocxx::pool::entry clientEntry = this->_pool.acquire();
	thread_local mongocxx::database db((*clientEntry)[DBConstants::DB_NAME]);
	return db;
}

string MongoDatabase::getHexPBKDFHash(string password, string salt, string pepper, int iterations)
{
	//Add a pepper to force attackers to create new rainbow tables for our db
	password += salt;
	password += pepper;
	std::vector<uint8_t> bytes(password.begin(), password.end());

	picosha2::hash256_one_by_one hasher;
	for (int i = 0; i < iterations; i++)
	{
		hasher.init();
		hasher.process(bytes.begin(), bytes.end());
		hasher.finish();
		//Make sure that we capture the full digest
		bytes.resize(picosha2::k_digest_size);
		hasher.get_hash_bytes(bytes.begin(), bytes.end());
	}

	return picosha2::bytes_to_hex_string(bytes);
}

bool MongoDatabase::doesUserExist(string username)
{
	mongocxx::collection userColl = this->_getDb()[DBConstants::USER_COLL];
	bool wasFound = false;
	try
	{
		wasFound = userColl.find_one(getDocument({ { DBConstants::USERNAME, username } })).has_value();
	}
	catch (const mongocxx::exception& e)
	{
		throw ServerError(Errors::MONGO_CONNECTION_ERROR);
	}
	return wasFound;
}

bool MongoDatabase::checkPassword(string username, string password)
{
	mongocxx::collection userColl = this->_getDb()[DBConstants::USER_COLL];
	string hash = MongoDatabase::getHexPBKDFHash(password, username);
	bool wasFound = false;
	try
	{
		wasFound = userColl.find_one(getDocument(
			{ { DBConstants::USERNAME, username },
			  { DBConstants::PASSWORD, hash} })).has_value();
	}
	catch (const mongocxx::exception& e)
	{
		throw ServerError(Errors::MONGO_CONNECTION_ERROR);
	}
	return wasFound;
}

void MongoDatabase::signupUser(string email, string username, string password)
{
	mongocxx::collection userColl = this->_getDb()[DBConstants::USER_COLL];
	string hash = MongoDatabase::getHexPBKDFHash(password, username);
	auto doc = getDocument({ {DBConstants::USERNAME, username},
							{DBConstants::EMAIL, email},
							{DBConstants::PASSWORD, hash} });

	try
	{
		auto insertionResult = userColl.insert_one(doc.view());
	}
	catch (const mongocxx::exception& e)
	{
		throw ServerError(Errors::MONGO_CONNECTION_ERROR);
	}
}

std::vector<highscore> MongoDatabase::getHighscores(unsigned int numScores)
{
	std::vector<highscore> highscoreVector;
	highscoreVector.reserve(numScores);
	mongocxx::collection scoreColl = this->_getDb()[DBConstants::SCORE_COLL];

	mongocxx::options::find scoreAscending{};
	scoreAscending.sort(getDocument({ {DBConstants::SCORE, MONGO_DESCENDING} }));

	try
	{
		mongocxx::cursor cur = scoreColl.find({}, scoreAscending);
		unsigned int numFound = 0;
		for (auto it = cur.begin(); it != cur.end() && numFound <= numScores; ++it)
		{
			auto& foundScore = *it;
			highscore highScore;
			try
			{
				//Get the username associated with the high score
				highScore.first = foundScore.find(DBConstants::USERNAME)->get_utf8().value.to_string();
				//Get the actual score
				highScore.second = foundScore.find(DBConstants::SCORE)->get_double().value;
			}
			catch (const bsoncxx::exception&)
			{
				throw ServerError(Errors::WRONG_TYPE_ERROR);
			}

			highscoreVector.push_back(highScore);
			numFound++;
		}
	}
	catch (const mongocxx::exception& e)
	{
		throw ServerError(Errors::MONGO_CONNECTION_ERROR);
	}

	return std::move(highscoreVector);
}

void MongoDatabase::registerHighscore(highscore score)
{
	//Make sure that the user exists - only perform this check in release mode, as we might wanna test random strings in debug.
#ifndef _DEBUG
	if (!this->doesUserExist(score.first))
	{
		throw ServerError(Errors::USER_DOESNT_EXIST);
	}
#endif

	mongocxx::collection scoreColl = this->_getDb()[DBConstants::SCORE_COLL];
	auto doc = getDocument({ {DBConstants::USERNAME, score.first},
							 {DBConstants::SCORE, score.second} });

	try
	{
		auto insertionResult = scoreColl.insert_one(doc.view());
	}
	catch (const mongocxx::exception& e)
	{
		throw ServerError(Errors::MONGO_CONNECTION_ERROR);
	}
}

std::vector<highscore> MongoDatabase::getHighscores(unsigned int numScores)
{
	std::vector<highscore> highscoreVector;
	highscoreVector.reserve(numScores);
	mongocxx::collection scoreColl = this->_getDb()[DBConstants::SCORE_COLL];

	mongocxx::options::find scoreAscending{};
	scoreAscending.sort(getDocument({ {DBConstants::SCORE, MONGO_DESCENDING} }));

	mongocxx::cursor cur = scoreColl.find({}, scoreAscending);
	
	unsigned int numFound = 0;
	for (auto it = cur.begin(); it != cur.end() && numFound <= numScores; ++it)
	{
		auto& foundScore = *it;
		highscore highScore;
		try
		{
			//Get the username associated with the high score
			highScore.first = foundScore.find(DBConstants::USERNAME)->get_utf8().value.to_string();
			//Get the actual score
			highScore.second = foundScore.find(DBConstants::SCORE)->get_double().value;
		}
		catch (const bsoncxx::exception&)
		{
			throw ServerError(Errors::WRONG_TYPE_ERROR);
		}
		
		highscoreVector.push_back(highScore);
		numFound++;
	}

	return std::move(highscoreVector);
}

void MongoDatabase::registerHighscore(highscore score)
{
	//Make sure that the user exists - only perform this check in release mode, as we might wanna test random strings in debug.
#ifndef _DEBUG
	if (!this->doesUserExist(score.first))
	{
		throw ServerError(Errors::USER_DOESNT_EXIST);
	}
#endif

	mongocxx::collection scoreColl = this->_getDb()[DBConstants::SCORE_COLL];
	auto doc = getDocument({ {DBConstants::USERNAME, score.first},
							 {DBConstants::SCORE, score.second} });

	auto insertionResult = scoreColl.insert_one(doc.view());
}

MongoDatabase& MongoDatabase::getInstance()
{
	static MongoDatabase instance;
	return instance;
}
