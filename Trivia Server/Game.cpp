#include "Game.h"

Game::Game(std::vector<Question> questions,RoomData roomdata) : _questions(questions) ,_roomData(roomdata)
{
	this->_questionIndex = 0;
	this->_startQuestionTime = time(&this->_startQuestionTime);
	this->_askedBefore = false;
}

Game::~Game()
{
}

Question& Game::getQuestionForUser(string User)
{
	updateCorrectQuestion(User);
	updateQuestion();
	//start question time
	if (!this->_askedBefore)
	{
		time(&this->_startQuestionTime);
	}
	else
	{
		// throw
	}
	updateQuestion();
	std::lock_guard<std::mutex> guard(this->_playersMutex);
	return this->_players[User].currentQuestion;
}

AnswerCodes Game::submitAnswer(unsigned int answer, string user)
{
	std::lock_guard<std::mutex> guard(this->_questionsMutex);
	if (answer == this->_questions[this->_questionIndex].getCorrectAnswerIndex())
	{
		if (updateQuestion())
		{
			//throw
		}
		std::lock_guard<std::mutex> guard(this->_playersMutex);
		this->_players[user].correctAnswerCount++;
		//Add the time to the average answer
		this->_players[user].averangeAnswerTime += getdiffTime();
		//can receive a new question
		this->_askedBefore = false;
		if (this->_roomData.questionCount > this->_questionIndex)
		{
			//throw
		}
		//If it's the last question
		if (this->_questions.size() == this->_questionIndex)
		{
			GameOver(user);
		}
		else
		{
			this->_players[user].currentQuestion = this->_questions[this->_questionIndex];
		}
		return CORRECT_ANS;
	}
	else
	{
		std::lock_guard<std::mutex> guard(this->_playersMutex);
		this->_players[user].wrongAnswerCount++;
		if (updateQuestion())
		{
			//throw
		}
		//Add the time to the average answer
		this->_players[user].averangeAnswerTime += getdiffTime();
		//can receive a new question
		this->_askedBefore = false;
		//If it's the last question
		if (this->_roomData.questionCount > this->_questionIndex)
		{
			//throw
		}
		if (this->_questions.size() == this->_questionIndex)
		{
			GameOver(user);
		}
		else
		{
			this->_players[user].currentQuestion = this->_questions[this->_questionIndex];
		}
		return INCORRECT_ANS;
	}
}

void Game::removePlayer(string user)
{
	std::lock_guard<std::mutex> guard(this->_playersMutex);
	this->_players.erase(user);
}

unsigned int Game::getCorrectId()
{
	std::lock_guard<std::mutex> guard(this->_questionsMutex);
	return this->_questions[this->_questionIndex].getCorrectAnswerIndex();
}

void Game::updateUsers(std::vector<LoggedUser> users)
{
	std::vector<string> s;
	Question q("bla",s,0);
	GameData data = {q,0,0,0};
	for (std::vector<LoggedUser>::iterator it = users.begin(); it != users.end(); ++it)
	{
		this->_players[*(it)] = data;
	}
}

unsigned int Game::getdiffTime()
{
	time_t now;
	time(&now);
	std::lock_guard<std::mutex> guard(this->_timeMutex);
	return int(difftime(this->_startQuestionTime,now));
}

void Game::GameOver(string user)
{
	//Avergae
	std::lock_guard<std::mutex> guard(this->_playersMutex);
	this->_players[user].averangeAnswerTime = (this->_players[user].averangeAnswerTime / this->_questions.size());
	//Game over
}

bool Game::updateQuestion()
{
	time_t now;
	time(&now);
	std::lock_guard<std::mutex> guard(this->_timeMutex);
	if (difftime(this->_startQuestionTime, now) > this->_roomData.timePerQuestion)
	{
		std::lock_guard<std::mutex> guard(this->_questionsMutex);
		this->_questionIndex++;
		return true;
	}
	return false;
}

void Game::updateCorrectQuestion(string user)
{
	std::lock_guard<std::mutex> guard(this->_playersMutex);
	this->_players[user].currentQuestion = this->_questions[this->_questionIndex];
}
