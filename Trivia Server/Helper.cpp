#include "Helper.h"

// recieves the type code of the message from socket
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, CODE_LEN);
	std::string msg(chars.get());

	if (msg == "")
	{
		return 0;
	}

	int res = atoi(chars.get());
	return res;
}

// receive data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, bytesNum);
	return atoi(chars.get());
}

// receive data from socket according to byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	std::unique_ptr<char[]> chars = getPartFromSocket(sc, bytesNum);
	string res(chars.get());
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}

// recieve data from socket according byteSize
std::unique_ptr<char[]> Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, MSG_WAITALL);
}

std::unique_ptr<char[]> Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{	
		return std::make_unique<char[]>(1);
	}

	std::unique_ptr<char[]> data = std::make_unique<char[]>(bytesNum + 1);
	int res = recv(sc, data.get(), bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = RECV_ERR;
		s += std::to_string(sc);
		throw socketRecvError(s.c_str());
	}

	data[bytesNum] = NULL; //Make sure that data ends with a null terminator
	return data;
}

// send data to socket
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw socketSendError(SEND_ERR);
	}
}