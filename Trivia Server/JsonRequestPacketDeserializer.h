#pragma once
#include "Structs.h"
#include "json.hpp"
#include "Constants.h"

using json = nlohmann::json;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(buffer bytes);
	static SignupRequest deserializeSignUpRequest(buffer bytes);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(buffer bytes);
	static JoinRoomRequest deserializeJoinRoomRequest(buffer bytes);
	static CreateRoomRequest deserializeCreateRoomRequest(buffer bytes);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(buffer bytes);
	//Some functions are just not implemented here, like logout, get high scores, and get rooms, because they don't have
	//any parameters. The only data is the actual command.
};

