#pragma once
#include <thread>
#include <iostream>
#include <ctime>

#include "Helper.h"
#include "WSAInitializer.h"
#include "RequestHandlerFactory.h"
#include "Structs.h"
#include "Constants.h"
#include "JsonResponsePacketSerializer.h"

#define SOCKET_CREATION_ERROR " - Failed to create the socket"

#define WRONG_CODE_ERROR "ERROR: That message is irrelevant to the current context."

class Communicator
{
public:
	Communicator(RequestHandlerFactory& handlerFactory);
	~Communicator();
	void bindAndListen(int port=DEFAULT_PORT);

private:
	bool _serverAlive;
	static WSAInitializer _wsa;
	SOCKET _listeningSocket;
	RequestHandlerFactory& _handlerFactory;
	void _handleSingleConnection(SOCKET sc);
};

