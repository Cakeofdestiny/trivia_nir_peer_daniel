#include "Communicator.h"

Communicator::Communicator(RequestHandlerFactory& handlerFactory) : _handlerFactory(handlerFactory) 
{
	//SOCK_STREAM & IPPROTO_TCP for a TCP socket
	this->_listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_listeningSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ SOCKET_CREATION_ERROR);

	this->_serverAlive = true;
}

Communicator::~Communicator()
{
	this->_serverAlive = false;
}


void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    

	if (::bind(this->_listeningSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(this->_listeningSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (this->_listeningSocket)
	{
		//Note that accept is blocking
		//this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(this->_listeningSocket, NULL, NULL);

		TRACE("Accepted a new socket");

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		// Handles the conversation with the client in a new thread
		//this->_handleSingleConnection(client_socket);
		std::thread(&Communicator::_handleSingleConnection, this, client_socket).detach();
	}

	TRACE("Finished listening")
}

void Communicator::_handleSingleConnection(SOCKET sc)
{
	std::unique_ptr<IRequestHandler> currentHandler = this->_handlerFactory.createLoginRequestHandler();
	bool socketAlive = true;
	try
	{
		while (socketAlive && this->_serverAlive)
		{
			Request request;
			int size = 0;
			try
			{
				size = Helper::getIntPartFromSocket(sc, SIZE_BYTES_LEN);
				request.id = Helper::getMessageTypeCode(sc);
			}
			catch (const socketRecvError&)
			{
				TRACE("%d closed the connection. Terminating...", sc); 
				request.id = REQ_EXIT;
			}
			
			if (request.id == REQ_EXIT)
			{
				currentHandler->terminateSession();
				socketAlive = false;
			}
			else if (currentHandler->isRequestRelevant(request))
			{
				request.receivalTime = std::time(nullptr);
				request.buffer.resize(size - CODE_LEN + 1, 0);
				socketAlive = recv(sc, (char*)request.buffer.data(), size - sizeof(request.id), MSG_WAITALL) != INVALID_SOCKET;
				RequestResult res;
				try
				{
					res = currentHandler->handleRequest(request);
				}
				catch (const UserError& e)
				{
					UserErrorResponse resp = { string(e.what()) };
					res.response = JsonResponsePacketSerializer::serializeResponse(resp);
				}
				catch (const ServerError& e)
				{
					ServerErrorResponse resp = { string(e.what()) };
					res.response = JsonResponsePacketSerializer::serializeResponse(resp);
					TRACE("Server error: %s", e.what());
				}

				//Change handler if the new handler isn't nullptr
				if (res.newHandler.get() != nullptr)
				{
					currentHandler = std::move(res.newHandler);
				}
				send(sc, (char*)res.response.data(), res.response.size(), 0);
			}
			else
			{
				UserErrorResponse resp = { WRONG_CODE_ERROR };
				TRACE("Code irrelevant. Code is %d", request.id);
				buffer respBytes = JsonResponsePacketSerializer::serializeResponse(resp);
				send(sc, (char*)respBytes.data(), respBytes.size(), 0);
			}
		}

		closesocket(sc);
	}
	//Avoid crashing the entire server when there's an unpredictable exception.
	catch (const std::exception& e)
	{
		TRACE("Unrecoverable Server Error. Terminating...\n Error: %s", e.what());
	}
	TRACE("Terminated a socket...");
}


