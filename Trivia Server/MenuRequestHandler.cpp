#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser Log_User,  HighscoreTable& highscoreTable,
	RequestHandlerFactory& requestHandler) : _user(Log_User) , _roomManager(RoomManager::getInstance())
	,_highscoreTable(highscoreTable), _handlerFactory(requestHandler)
{
}

bool MenuRequestHandler::isRequestRelevant(Request req)
{
	return (req.id == REQ_SIGNOUT) || (req.id == REQ_ROOM_LIST) || (req.id == REQ_JOIN) ||
		(req.id == REQ_HIGH_SCORES) || (req.id == REQ_CREATE) || (req.id == REQ_LIST_IN_ROOM);
}

RequestResult MenuRequestHandler::handleRequest(Request req)
{
	switch (req.id)
	{
	case REQ_SIGNOUT:
		return signout(req);
		break;
	case REQ_ROOM_LIST:
		return getRooms(req);
		break;
	case REQ_JOIN:
		return joinRoom(req);
		break;
	case REQ_HIGH_SCORES:
		return getHighscores(req);
		break;
	case REQ_CREATE:
		return createRoom(req);
		break;
	case REQ_LIST_IN_ROOM:
		return getPlayersInRoom(req);
		break;
	default:
		throw ServerError(Errors::ID_MISMATCH);
		break;
	}
}

void MenuRequestHandler::terminateSession()
{
	this->_handlerFactory.getLoginManager().logout(this->_user);
}

RequestResult MenuRequestHandler::signout(Request req)
{
	RequestResult reqResult;
	LogoutResponse logout_resp;
	std::lock_guard<std::mutex> guard(this->_handlerFactoryMutex);
	this->_handlerFactory.getLoginManager().logout(this->_user);
	reqResult.newHandler = this->_handlerFactory.createLoginRequestHandler();
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(logout_resp);
	return reqResult;
}

RequestResult MenuRequestHandler::getRooms(Request req)
{
	GetRoomsResponse room_resp;
	//lock
	std::lock_guard<std::mutex> guard(this->_roomManagerMutex);
	//get rooms
	room_resp.rooms = this->_roomManager.getRooms();
	//return result
	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(room_resp);
	return reqResult;
}

RequestResult MenuRequestHandler::getPlayersInRoom(Request req)
{
	//request
	GetPlayersInRoomRequest players_req = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(req.buffer);
	RequestResult reqResult;
	//Response
	GetPlayersInRoomResponse players_response;
	//get list of players
	players_response.names = getListPlayersInRoom(players_req.roomId);
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(players_response);
	return reqResult;
}

RequestResult MenuRequestHandler::getHighscores(Request req)
{
	RequestResult reqResult;
	HighscoreResponse highscore_resp;
	//TODO: Implement error checking with the database
	highscore_resp.highscores = this->_highscoreTable.getHighscores();
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(highscore_resp);
	return reqResult;
}

RequestResult MenuRequestHandler::joinRoom(Request req)
{
	//get request
	JoinRoomRequest join_req = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(req.buffer);
	RequestResult reqResult;
	//lock
	std::lock_guard<std::mutex> guard(this->_roomManagerMutex);
	RoomPtr roomToJoin = this->_roomManager.getRoomByID(join_req.roomId);
	JoinRoomResponse join_resp;
	roomToJoin->addUser(this->_user);
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(join_resp);
	reqResult.newHandler = this->_handlerFactory.createRoomMemberRequestHandler(this->_user, roomToJoin);
	return reqResult;
}

RequestResult MenuRequestHandler::createRoom(Request req)
{
	//create room request
	CreateRoomRequest createReq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(req.buffer);
	RequestResult reqResult;
	CreateRoomResponse create_resp;

	if (createReq.maxUsers == 0 || createReq.questionCount == 0 || createReq.roomName == "" || createReq.answerTimeOut == 0)
	{
		throw UserError(Errors::INVALID_ROOM_PARAMETERS);
	}

	//get status
	RoomPtr createdRoom = this->_roomManager
		.createRoom(createReq.roomName, createReq.maxUsers, createReq.questionCount, createReq.answerTimeOut, ROOM_INACTIVE);
	createdRoom->addUser(this->_user);
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(create_resp);
	std::lock_guard<std::mutex> guard(this->_handlerFactoryMutex);
	reqResult.newHandler = this->_handlerFactory.createRoomAdminRequestHandler(this->_user, createdRoom);
	return reqResult;
}

std::vector<std::string> MenuRequestHandler::getListPlayersInRoom(unsigned int id)
{
	std::vector<std::string> names = std::vector<std::string>();
	std::vector<LoggedUser> res;
	//Get map
	std::lock_guard<std::mutex> guard(this->_roomManagerMutex);
	for (auto& it : this->_roomManager.getRoomMap())
	{
		if (it.first == id)
		{
			res = it.second->getAllUsers();
		}
	}
	//Get names
	for (auto it = res.begin(); it != res.end(); it++)
	{
		names.push_back(*it);
	}
	return names;
}
