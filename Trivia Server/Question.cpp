#include "Question.h"

Question::Question(string question, std::vector<string>& possibleAnswers, unsigned int CorrectAnswerIndex) : _question(question), _possibleAnswers(possibleAnswers),_CorrectAnswerIndex(CorrectAnswerIndex)
{

}

Question::~Question()
{
}

Question::Question(const Question& other)
{
	this->_question = other._question;
	this->_possibleAnswers = other._possibleAnswers;
	this->_CorrectAnswerIndex = other._CorrectAnswerIndex;
}

Question::Question()
{
	std::vector<string> s;
	this->_question = "";
	this->_possibleAnswers = s;
	this->_CorrectAnswerIndex = 0;
}

string Question::getQuestion()
{
	return this->_question;
}

std::vector<string> Question::getPossibleAnswers()
{
	return this->_possibleAnswers;
}

unsigned int Question::getCorrectAnswerIndex()
{
	return this->_CorrectAnswerIndex;
}
