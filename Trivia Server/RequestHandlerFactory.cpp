#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase& database) : _database(database)
, _loginManager(database), _highscoreTable(database)
{
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->_loginManager;
}



std::unique_ptr<IRequestHandler> RequestHandlerFactory::createLoginRequestHandler()
{
	return std::make_unique<LoginRequestHandler>(this->_loginManager, *this);
}

std::unique_ptr<IRequestHandler> RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return std::make_unique<MenuRequestHandler>(user, this->_highscoreTable, *this);
}

std::unique_ptr<IRequestHandler> RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, RoomPtr room)
{
	return std::make_unique<RoomMemberRequestHandler>(user, *this, room);
}

std::unique_ptr<IRequestHandler> RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, RoomPtr room)
{
	return std::make_unique<RoomAdminRequestHandler>(user, *this, room);
}

std::unique_ptr<IRequestHandler> RequestHandlerFactory::createGameRequestHandler(LoggedUser user, GamePtr game)
{
	return std::make_unique<GameRequestHandler>(game,user,*this);
}
