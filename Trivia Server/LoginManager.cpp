#include "LoginManager.h"

LoginManager::LoginManager(IDatabase& database) : _database(database) 
{
	 
}

LoginManager::~LoginManager()
{
}

void LoginManager::signup(string email, string username, string password)
{
	if (this->_database.doesUserExist(username))
	{
		throw UserError(Errors::USER_ALREADY_EXISTS);
	}
	{
		std::lock_guard<std::mutex> lock(this->_dataMutex);
		this->_loggedUsers.push_back(LoggedUser(username));
	}
	this->_database.signupUser(email,username,password);
}

bool LoginManager::login(string username, string password)
{
	//If the user is not already logged in and if their details check out on the database
	bool loginSuccess = !this->_checkIfLogged(username) && this->_database.checkPassword(username, password);
	if (loginSuccess)
	{
		std::lock_guard<std::mutex> lock(this->_dataMutex);
		this->_loggedUsers.push_back(username);
	}
	return loginSuccess;
}

void LoginManager::logout(string username)
{
	std::lock_guard<std::mutex> lock(this->_dataMutex);
	//delete from vector
	size_t oldSize = this->_loggedUsers.size();
	this->_loggedUsers.erase(
		std::remove_if(this->_loggedUsers.begin(), this->_loggedUsers.end(),
			[username](const LoggedUser & user) {return user == username;}));
}

bool LoginManager::_checkIfLogged(string username)
{
	std::lock_guard<std::mutex> lock(this->_dataMutex);
	std::vector<LoggedUser>::iterator it = this->_loggedUsers.begin();
	for (; it != this->_loggedUsers.end(); ++it)
	{
		if (*it == username)
		{
			return true;
		}
	}
	return false;
}