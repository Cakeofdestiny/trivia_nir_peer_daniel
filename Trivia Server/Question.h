#pragma once
#include <iostream>
#include <vector>
#include "exceptions.h"
#include "Constants.h"

using namespace std;

class Question
{
public:
	Question(string question, std::vector<string>& possibleAnswers, unsigned int CorrentAnswerIndex);
	~Question();
	Question(const Question& other);
	Question();

	string getQuestion();
	std::vector<string> getPossibleAnswers();
	unsigned int getCorrectAnswerIndex();

private:
	string _question;
	std::vector<string> _possibleAnswers;
	unsigned int _CorrectAnswerIndex;
};