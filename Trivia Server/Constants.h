#pragma once

enum RequestCodes {	REQ_LOGIN=01,REQ_SIGNUP=02, REQ_SIGNOUT=03, REQ_EXIT=04, //Login
					REQ_ROOM_LIST=10, REQ_HIGH_SCORES=11, REQ_JOIN=12, REQ_CREATE=13,  //Menu
					REQ_LIST_IN_ROOM=20, REQ_LEAVE_ROOM=21, REQ_GET_STATE=22, //Room Member
					REQ_CLOSE_ROOM=30, REQ_START_GAME=31, //Room Admin
					REQ_QUESTION,REQ_ANSWER,REQ_RESULTS,REQ_LEAVE //Game
					};

enum StatusCodes	{RESP_OK=20, RESP_USER_ERROR=40, RESP_SERVER_ERROR=50};
enum AnswerCodes    {CORRECT_ANS=1, INCORRECT_ANS=0};

#define MONGO_USER "cyberuser"
#define MONGO_PASSWORD "VGs7talimuLyIWj0"

#define MIN_POOL_CONNS "5"

#define MONGO_URI "mongodb+srv://" MONGO_USER ":" MONGO_PASSWORD "@triviacluster-sm3b6.mongodb.net/test?retryWrites=true&minPoolSize=" MIN_POOL_CONNS

#define PEPPER "LaQBYbhqe2qz"

#define DEFAULT_PORT 1337

#define DEFAULT_HIGHSCORE_LIMIT 10

//For the helper

#define CODE_LEN 2
#define SIZE_BYTES_LEN 4

//This is obviously still very insecure (should be over 10,000), but I wanted to avoid the Debug speed penalty.
//No one should put any actual important data in this app anyway.
#define HASH_ITERATIONS 1000

#define ROOM_CLOSED -1
#define ROOM_INACTIVE 0
#define ROOM_PLAYING 1

#define MONGO_ASCENDING 1
#define MONGO_DESCENDING -1

#define FIRST_ANSWER 0
#define SECOND_ANSWER 1
#define THIRD_ANSWER 2
#define FOURTH_ANSWER 3

namespace Errors
{
	constexpr char ROOM_NOT_FOUND[] = "Error: The room that the user asked for was not found.";
	constexpr char EXCEEDED_MAX_USERS[] = "Error: The room already has the maximum amount of players.";
	constexpr char INVALID_LOGIN[] = "Error: Either the username or password were wrong.";
	constexpr char SERVER_SIGNOUT_ERR[] = "Error: I couldn't sign you out. It's probably safe to quit manually.";
	constexpr char USER_ALREADY_EXISTS[] = "Error: You tried to sign up as a user that already exists.";
	constexpr char INVALID_ROOM_PARAMETERS[] = "Error: The room parameters are invalid.";
	constexpr char USER_DOESNT_EXIST[] = "Server Error: The system tried to register a highscore for an inexistant user.";
	constexpr char WRONG_TYPE_ERROR[] = "Server Error: The value was of the wrong type. Internal database error.";
	constexpr char ID_MISMATCH[] = "Server Error: ID Mismatch between internal lists.";
	constexpr char MONGO_CONNECTION_ERROR[] = "Server Error: Could not connect to the database.";
}

namespace DBConstants
{
	constexpr char DB_NAME[] = "Trivia";
	constexpr char USER_COLL[] = "Users";
	constexpr char SCORE_COLL[] = "Scores";

	constexpr char USERNAME[] = "username";
	constexpr char EMAIL[] = "email";
	constexpr char PASSWORD[] = "password_hashed";
	constexpr char SCORE[] = "score";
}

namespace RespFields
{
	constexpr char ERR[] = "error";
	constexpr char STATUS[] = "status";
	constexpr char ROOMS[] = "rooms";
	constexpr char NAMES[] = "names";
	constexpr char HIGHSCORES[] = "highscores";
	constexpr char HIGHSCORE_USER[] = "user";
	constexpr char HIGHSCORE_SCORE[] = "score";
	constexpr char PLAYERS[] = "players";
	constexpr char GAME_STATUS[] = "gameStatus";
	constexpr char QUESTION_COUNT[] = "questionCount";
	constexpr char TIMEOUT[] = "answerTimeOut";

	constexpr char ROOM_ID[] = "id";
	constexpr char ROOM_NAME[] = "name";
	constexpr char ROOM_MAX_PLAYERS[] = "maxPlayers";
	constexpr char ROOM_TIME_PER_Q[] = "timePerQuestion";
	constexpr char ROOM_QUESTION_COUNT[] = "questionCount";
	constexpr char ROOM_ACTIVE[] = "isActive";

	constexpr char ANS_ID[] = "correctAnswerId";
	constexpr char QUESTION[] = "question";

	constexpr char RESULTS[] = "results";
	constexpr char RESULTS_USERNAME[] = "username";
	constexpr char RESULTS_CORRECT_ANSWER_COUNT[] = "correctAnswerCount";
	constexpr char RESULTS_WRONG_ANSWER_COUNT[] = "wrongAnswerCount";
	constexpr char RESULTS_AVERAGE_ANSWER_TIME[] = "averageAnswerTime";
	constexpr char ANSWERS[] = "answers";
}

namespace ReqFields
{
	constexpr char USERNAME[] = "username";
	constexpr char PASSWORD[] = "password";
	constexpr char EMAIL[] = "email";

	constexpr char ROOM_ID[] = "roomId";
	constexpr char ROOM_NAME[] = "roomName";
	constexpr char MAX_USERS[] = "maxUsers";
	constexpr char QUESTIONS[] = "questionCount";
	constexpr char TIMEOUT[] = "answerTimeOut";
	constexpr char ANS_ID[] = "answerId";
}
