#pragma once

#include "LoginManager.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "HighscoreTable.h"
#include "RoomManager.h"
#include "Structs.h"
#include <mutex>
#include <vector>

class MenuRequestHandler : public IRequestHandler
{
	
public:

	MenuRequestHandler(LoggedUser user, HighscoreTable& highscoreTable, RequestHandlerFactory& requestHandler);
	~MenuRequestHandler() = default;

	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request req);
	virtual void terminateSession();

private:
	std::vector<std::string> getListPlayersInRoom(unsigned int id);

	RequestResult signout(Request req);
	RequestResult getRooms(Request req);
	RequestResult getPlayersInRoom(Request req);
	RequestResult getHighscores(Request req);
	RequestResult joinRoom(Request req);
	RequestResult createRoom(Request req);

	LoggedUser _user;
	RoomManager& _roomManager;
	HighscoreTable& _highscoreTable;
	RequestHandlerFactory& _handlerFactory;
	std::mutex _handlerFactoryMutex;
	std::mutex _roomManagerMutex;
};

