#pragma once
#include "Structs.h"
#include "Constants.h"
#include "exceptions.h"
#include <iostream>
#include <deque>
#include <algorithm>
#include <mutex>
#include "Game.h"

typedef std::shared_ptr<Game> GamePtr;

class Room
{
public:
	Room(unsigned int id, string name, int maxPlayers, int questionCount, int timePerQuestion, int isActive);
	Room(const Room& another);
	Room() = default;
	~Room() = default;

	void addUser(std::string name);
	void removeUser(std::string username);
	std::vector<LoggedUser> getAllUsers();
	// 1 - Game is active
	// 0 - Game is not active
	// -1 - Game is not available
	void setRoomStatus(int stat);
	const RoomData& getRoomData();
	unsigned int getRoomID();
	void setGamePtr(GamePtr game);

private:
	std::vector<LoggedUser> _users;
	RoomData _metadata;
	std::mutex _dataMutex;
	GamePtr _game;
};

