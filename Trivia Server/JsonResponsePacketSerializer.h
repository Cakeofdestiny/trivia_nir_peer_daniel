#pragma once

#include "Structs.h"
#include "Constants.h"
#include "json.hpp"
#include "Helper.h"

using json = nlohmann::json;

class JsonResponsePacketSerializer
{
public:
	//Serialize an OK response, without any additional data. Useful for cases where the only data is op success/failure.
	static buffer serializeOK();
	static buffer serializeResponse(UserErrorResponse resp);
	static buffer serializeResponse(ServerErrorResponse resp);
	static buffer serializeResponse(LoginResponse resp);
	static buffer serializeResponse(SignupResponse resp);
	static buffer serializeResponse(LogoutResponse resp);
	static buffer serializeResponse(GetRoomsResponse resp);
	static buffer serializeResponse(GetPlayersInRoomResponse resp);
	static buffer serializeResponse(JoinRoomResponse resp);
	static buffer serializeResponse(CreateRoomResponse resp);
	static buffer serializeResponse(HighscoreResponse resp);
	static buffer serializeResponse(CloseRoomResponse resp);
	static buffer serializeResponse(StartGameResponse resp);
	static buffer serializeResponse(GetRoomStateResponse resp);
	static buffer serializeResponse(LeaveRoomResponse resp);
	static buffer serializeResponse(GetQuestionResponse);
	static buffer serializeResponse(SubmitAnswerResponse);
	static buffer serializeResponse(LeaveGameResponse);
	static buffer serializeResultsResponse(GetGameResultsResponse);
private:
	static buffer _createBuffer(json& responseJson, unsigned int code=RESP_OK);
};

