#pragma once
#include "Structs.h"

//Request handler interface
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(Request request) = 0;
	virtual RequestResult handleRequest(Request request) = 0;
	//Used when we want to terminate the current session for the user and do some cleanup.
	virtual void terminateSession() {};
};