#include "RoomManager.h"

RoomManager& RoomManager::getInstance()
{
	static RoomManager instance;
	return instance;
}

RoomManager::RoomManager()
{
	std::srand(static_cast<unsigned int>(time(NULL)));
}

RoomPtr RoomManager::createRoom(const string& name, int maxPlayers, int questionCount, unsigned int timePerQuestion, int isActive)
{
	RoomPtr newRoom = std::make_shared<Room>(this->_getFreeId(), name, maxPlayers, questionCount, timePerQuestion, isActive);
	//Lock
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	//Insert the new room
	this->_rooms[newRoom->getRoomData().id] = newRoom;
	return newRoom;
}

void RoomManager::deleteRoom(unsigned int roomId)
{
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	//delete required room
	this->_rooms.erase(roomId);
}

int RoomManager::getRoomState(unsigned int id)
{
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	//get room state
	return this->_rooms[id]->getRoomData().isActive;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomList;
	//Return room list
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	for (auto& it = this->_rooms.begin(); it != this->_rooms.end(); ++it)
	{
		roomList.push_back(it->second->getRoomData());
	}
	return roomList;
}

const RoomMap& RoomManager::getRoomMap()
{
	return this->_rooms;
}

RoomPtr RoomManager::getRoomByID(unsigned int id)
{
	std::lock_guard<std::mutex> guard(this->_roomMutex);
	auto roomIt = this->_rooms.find(id);
	//If we didn't find it
	if (roomIt == this->_rooms.end())
	{
		throw UserError(Errors::ROOM_NOT_FOUND);
	}
	return roomIt->second;
}

unsigned int RoomManager::_getFreeId()
{
	
	unsigned int first, second, third;
	unsigned int id = 0;

	//Roll a random number until it doesn't collide. 
	while (this->_rooms.find(id) != this->_rooms.end())
	{
		//"Augment" our random function
		first = rand() % 0xFFFF;
		second = rand() % (0xFFFF - 1);
		third = rand() % 0xFFFF;
		id = (first*second) + third;
	}
	return id;
}
