#pragma once
#include <iostream>
#include "Structs.h"
#include "Question.h"
#include "Constants.h"
#include "Game.h"
#include "RequestHandlerFactory.h"
#include <mutex>
#include <vector>

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(GamePtr game,LoggedUser& user,RequestHandlerFactory& handlerFactory);
	~GameRequestHandler();
	GameRequestHandler() = default;

	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request req);

private:
	RequestResult getQuestion(Request req);
	RequestResult submitAnswer(Request req);
	RequestResult getGameResults(Request req);
	RequestResult leaveGame(Request req);
	std::map<unsigned int, string> updatePossibleAnswers(std::vector<string> answers);

	GamePtr _game;
	LoggedUser& _user;
	RequestHandlerFactory& _handlerFactory;
	std::mutex _gameMutex;
};