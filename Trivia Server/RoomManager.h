#pragma once
#include "exceptions.h"
#include "Structs.h"
#include <unordered_map>
#include <mutex>
#include <utility>
#include <cstdlib>
#include <memory>
#include "Room.h"

typedef std::shared_ptr<Room> RoomPtr;
typedef std::unordered_map<unsigned int, RoomPtr> RoomMap;


class RoomManager
{
public:
	static RoomManager& getInstance();

	//Returns status
	RoomPtr createRoom(const string& name, int maxPlayers, int questionCount, unsigned int timePerQuestion, int isActive);
	void deleteRoom(unsigned int roomId);
	int getRoomState(unsigned int id);
	std::vector<RoomData> getRooms();
	const RoomMap& getRoomMap();
	RoomPtr getRoomByID(unsigned int id);
	

private:
	RoomManager();

	unsigned int _getFreeId();
	std::mutex _roomMutex;
	RoomMap _rooms;
};