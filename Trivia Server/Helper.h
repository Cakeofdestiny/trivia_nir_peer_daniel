#pragma once

#include "Constants.h"
#include "WSAInitializer.h"
#include "exceptions.h"

#include <memory>
#include <string>
#include <sstream>
#include <iomanip>

#define SEND_ERR "I encountered an error while sending a message to the client."
#define RECV_ERR "I encountered an error while receiving a message from the client: "

using std::string;

class Helper
{
public:
	static int getMessageTypeCode(SOCKET sc);
	//Translates from a string
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum, int flags);
	static void sendData(SOCKET sc, std::string message);
	static std::string getPaddedNumber(int num, int digits);

	static std::unique_ptr<char[]> getPartFromSocket(SOCKET sc, int bytesNum);
	static std::unique_ptr<char[]> getPartFromSocket(SOCKET sc, int bytesNum, int flags);
};


#ifdef _DEBUG 
#include <stdio.h> 
// Basically printf with a newline
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#else // Override trace to do nothing in any version other than Debug
#define TRACE(msg, ...)
#endif