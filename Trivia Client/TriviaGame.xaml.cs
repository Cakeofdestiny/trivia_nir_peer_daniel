﻿using MaterialDesignThemes.Wpf;
using System;
using System.Text;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for TriviaGame.xaml
	/// </summary>
	public partial class TriviaGame : Window
	{
        private readonly ServerConnection conn = ServerConnection.Instance;
        private Card currentSelection = null;
		private Brush cachedBackgroundColor = null;
		private DispatcherTimer dispatcherTimer = new DispatcherTimer();
		private Brush cachedTimerForegroundColor = null;
		private int currentTime;
        private int numQuestions;
		//temp
		private RoomData room = new RoomData() { timePerQuestion = 10 };
		public TriviaGame(RoomData room)
		{
            numQuestions = 0;
            InitializeComponent();
		}

		public TriviaGame()
		{
            numQuestions = 0;
            InitializeComponent();
			List<AnswerItem> items = new List<AnswerItem>
			{
				new AnswerItem() {Answer="George H.W. Bush"},
				new AnswerItem() {Answer="Lyndon B. Johnson"},
				new AnswerItem() {Answer="George W. Bush"},
				new AnswerItem() {Answer="Bill Clinton"}
			};
			AnswerItemControl.ItemsSource = items;
			PlayerNameRun.Text = TriviaMenu.username;
			dispatcherTimer.Tick += answerTimer_Tick;
			//Ticks every second
			dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
			dispatcherTimer.Start();
			cachedTimerForegroundColor = TimerTextBox.Foreground;
			currentTime = room.timePerQuestion;
			TimerTextBox.Text = currentTime.ToString();
		}

		public class AnswerItem
		{
			public string Answer { get; set; }
		}

		private void AnswerCard_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (cachedBackgroundColor == null)
			{
				cachedBackgroundColor = (sender as Card).Background;
			}

			if (currentSelection != null && sender as Card != currentSelection)
			{
				currentSelection.Background = cachedBackgroundColor;
			}
			currentSelection = sender as Card;
			currentSelection.Background = System.Windows.Media.Brushes.Teal;
		}

		private void SubmitAnswerToServer()
		{
			var item = (currentSelection as FrameworkElement).DataContext;
			int index = AnswerItemControl.Items.IndexOf(item);
            SubmitAnswerRequest ans = new SubmitAnswerRequest();
            ans.answerId = index;
            SubmitAnswerResponse resp = conn.SubmitAnswer(ans);
            //Need to write more functions that will update the answers
            //and will show the correct and the wrong answer
            if(resp.status == Constants.CORRECT_ANS)
            {
                //Correct answer
            }
            else
            {
                //Wrong answer
            }
            //resp.correctAnswerId = Correct answer
        }

        private void UpdateQuestion()
        {
            if (numQuestions == room.questionCount)
            {

            }
            else
            {
                //Need to write more functions that will update the question.
                GetQuestionResponse rsp = conn.GetQuestion();
                numQuestions++;
                currentTime = room.timePerQuestion;
            }
        }
		private void answerTimer_Tick(object sender, EventArgs e)
		{
			currentTime--;
			TimerTextBox.Text = currentTime.ToString();
			if (currentTime == 0)
			{
				TimerTextBox.Foreground = cachedTimerForegroundColor;
				dispatcherTimer.Stop();
                //Insert submit answer and next question logic here
                SubmitAnswerToServer();
                UpdateQuestion();
			}
			else if (currentTime <= Constants.AnswerWarningTime)
			{
				TimerTextBox.Foreground = Brushes.Red;
			}
		}
	}
}

