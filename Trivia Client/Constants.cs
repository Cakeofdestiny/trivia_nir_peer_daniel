using System.Windows;

namespace Trivia_Client
{
	public static class Constants
	{
		public readonly static Thickness LoginMenuIconMargin = new Thickness(5, 17, 5, 0);

		public readonly static Thickness CreateRoomMenuIconMargin = new Thickness(5, 22, 5, 0);

		public readonly static Thickness HamburgerMenuMargin = new Thickness(16, 0, 32, 0);

		public const double HamburgerMenuFontSize = 18;

		public const double MenuWidth = 475;
		public const double FrameHeight = 550;

		public const string Bronze = "#CD7F32";

		public const int DelayMs = 2500;
		public const int IngameDelayMs = 500;

		public const int CodeLen = 2;
		public const int SizeLen = 4;

		public const int Port = 1337;

        public const int CORRECT_ANS = 1;
        public const int INCORRECT_ANS = 0;

        public static readonly string PortString = Port.ToString();
		public const string ServerIP = "127.0.0.1";

		public const string NotConnectedError = "Failed to connect to the server.";

		public enum RequestCodes
		{
			REQ_LOGIN = 01, REQ_SIGNUP = 02, REQ_SIGNOUT = 03, REQ_EXIT = 04, //Login
			REQ_ROOM_LIST = 10, REQ_HIGH_SCORES = 11, REQ_JOIN_ROOM = 12, REQ_CREATE_ROOM = 13,  //Menu
			REQ_LIST_ROOM_PLAYERS = 20, REQ_LEAVE_ROOM = 21, REQ_GET_STATE = 22, //Room Member
			REQ_CLOSE_ROOM = 30, REQ_START_GAME = 31, //Room Admin
            REQ_QUESTION = 40, REQ_ANSWER = 41, REQ_RESULTS = 42 //Game
		};

		public enum StatusCodes { RESP_OK = 20, RESP_USER_ERROR = 40, RESP_SERVER_ERROR = 50 };
		public enum GameStatus { GAME_CLOSED = -1, GAME_INACTIVE = 0, GAME_PLAYING = 1 };

        public const string WrongAnswer = "DarkRed";
		public const string RightAnswer = "DarkGreen";
		public const string SelectedAnswer = "Teal";

		public const int AnswerWarningTime = 3;

	}
}
