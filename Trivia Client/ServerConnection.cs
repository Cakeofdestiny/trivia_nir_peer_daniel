﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Trivia_Client
{
	sealed class ServerConnection
	{
		private Socket Sock;
		private static ServerConnection instance = null;
		private static readonly object InstanceLock = new object();
		private static readonly object SocketLock = new object();

		private ServerConnection() { }
		~ServerConnection()
		{
			if (!Sock.Equals(null))
			{
				Sock.Shutdown(SocketShutdown.Both);
				Sock.Close();
			}
		}

		public static ServerConnection Instance
		{
			get
			{
				lock (InstanceLock)
				{
					if (instance == null)
					{
						instance = new ServerConnection();
					}
					return instance;
				}
			}
		}

		public void Connect(string serverIPAddr, int port)
		{
			// Connect to a remote device.  
			try
			{
				IPAddress ServerIP = IPAddress.Parse(serverIPAddr);
				IPEndPoint remoteEP = new IPEndPoint(ServerIP, port);

				// Create a TCP/IP  socket.  
				Sock = new Socket(ServerIP.AddressFamily,
					SocketType.Stream, ProtocolType.Tcp);
				Sock.Connect(remoteEP);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				throw new InternalException(Constants.NotConnectedError);
			}
		}

		public void Login(LoginRequest req)
		{
			byte[] msg = JsonSerializer.SerializeLogin(req);
			SendMessage(msg);
			GetResponse();
		}
		public void Signup(SignupRequest req)
		{
			byte[] msg = JsonSerializer.SerializeSignup(req);
			SendMessage(msg);
			GetResponse();
		}
		public void CreateRoom(CreateRoomRequest req)
		{
			byte[] msg = JsonSerializer.CreateRoomSerializer(req);
			SendMessage(msg);
			GetResponse();
		}
		public void JoinRoom(JoinRoomRequest req)
		{
			byte[] msg = JsonSerializer.JoinRoomSerializer(req);
			SendMessage(msg);
			GetResponse();
		}
		public GetPlayersInRoomResponse GetPlayers(GetPlayersInRoomRequest req)
		{
			byte[] msg = JsonSerializer.ListPlayersInRoomSerializer(req);
			SendMessage(msg);
			return JsonDeserializer.Deserialize<GetPlayersInRoomResponse>(GetResponse().buff);
		}
		public void Signout()
		{
			byte[] msg = JsonSerializer.LogoutSerializer();
			SendMessage(msg);
			GetResponse();
		}
		public HighscoreResponse GetHighscores()
		{
			byte[] msg = JsonSerializer.ViewHighScoresSerializer();
			SendMessage(msg);
			return JsonDeserializer.Deserialize<HighscoreResponse>(GetResponse().buff);
		}
		public GetRoomsResponse GetRoomList()
		{
			byte[] msg = JsonSerializer.ViewRoomListSerializer();
			SendMessage(msg);
			return JsonDeserializer.Deserialize<GetRoomsResponse>(GetResponse().buff);
		}
		public GetRoomStateResponse GetRoomState()
		{
			byte[] msg = JsonSerializer.GetRoomStateSerializer();
			SendMessage(msg);
			return JsonDeserializer.Deserialize<GetRoomStateResponse>(GetResponse().buff);
		}
		public void LeaveRoom()
		{
			byte[] msg = JsonSerializer.LeaveRoomSerializer();
			SendMessage(msg);
			GetResponse();
		}
		public void CloseRoom()
		{
			byte[] msg = JsonSerializer.CloseRoomSerializer();
			SendMessage(msg);
			GetResponse();
		}
		public void StartRoom()
		{
			byte[] msg = JsonSerializer.StartRoomSerializer();
			SendMessage(msg);
			GetResponse();
		}

        public SubmitAnswerResponse SubmitAnswer(SubmitAnswerRequest answer)
        {
            byte[] msg = JsonSerializer.SubmitAnswerSerializer(answer);
            SendMessage(msg);
            return JsonDeserializer.Deserialize<SubmitAnswerResponse>(GetResponse().buff);
        }

        public GetQuestionResponse GetQuestion()
        {
            byte[] msg = JsonSerializer.GetQuestionSerializer();
            SendMessage(msg);
            return JsonDeserializer.Deserialize<GetQuestionResponse>(GetResponse().buff);
        }

        public GetGameResultsResponse GetResults()
        {
            byte[] msg = JsonSerializer.GetResultsSerializer();
            SendMessage(msg);
            return JsonDeserializer.Deserialize<GetGameResultsResponse>(GetResponse().buff);
        }

        private void AssertSocketConnected()
		{
			if (Sock == null)
			{
				throw new InternalException(Constants.NotConnectedError);
			}
		}


		//Creates and sends a response buffer, adding the size in the process too.
		private int SendMessage(byte[] data)
		{
			lock (SocketLock)
			{
				AssertSocketConnected();
				return Sock.Send(data);
			}
		}

		private int GetIntFromSocket(int length)
		{
			byte[] NumBytes = new byte[length];
			Sock.Receive(NumBytes, length, SocketFlags.None);
			return GUIHelper.TryParseInt(Encoding.UTF8.GetString(NumBytes, 0, length));
		}

		private Response GetResponse()
		{
			lock (SocketLock)
			{
				Response Resp;
				AssertSocketConnected();

				int ResponseSize = GetIntFromSocket(Constants.SizeLen);
				int ResponseCode = GetIntFromSocket(Constants.CodeLen);

				byte[] ResponseBytes = new byte[ResponseSize];
				if (ResponseSize != 0)
				{
					Sock.Receive(ResponseBytes, ResponseSize, SocketFlags.None);
				}

				if (ResponseCode != (int)Constants.StatusCodes.RESP_OK)
				{
					if (ResponseCode == (int)Constants.StatusCodes.RESP_USER_ERROR)
					{
						throw new UserException(JsonDeserializer.Deserialize<UserErrorResponse>(ResponseBytes).error);
					}
					else if (ResponseCode == (int)Constants.StatusCodes.RESP_SERVER_ERROR)
					{
						throw new ServerException(JsonDeserializer.Deserialize<ServerErrorResponse>(ResponseBytes).error);
					}
					else
					{
						throw new InternalException("Unidentified Response Code");
					}
				}

				Resp.code = ResponseCode;
				Resp.buff = ResponseBytes;

				return Resp;
			}
		}
	}
}
