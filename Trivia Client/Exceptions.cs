﻿using System;

namespace Trivia_Client
{
	//Used for code 400 responses from the server
	public class TriviaException : Exception
	{
		public TriviaException(string message) : base(message) { }
	}

	public class UserException : TriviaException
	{
		public UserException(string message) : base(message) { }
	}

	//Used for code 500 responses from the server
	public class ServerException : TriviaException
	{
		public ServerException(string message) : base(message) { }
	}

	public class InternalException : TriviaException
	{
		public InternalException(string message) : base(message) { }
	}
}
