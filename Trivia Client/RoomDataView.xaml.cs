﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for RoomDataView.xaml
	/// </summary>
	public partial class RoomDataView : Page
	{
		private RoomData _Room;
		public RoomData Room
		{
			get { return _Room; }
			set
			{
				_Room = value;
				Rebind();
			}
		}

		public List<string> Users
		{
			set { UserList.ItemsSource = value; }
		}

		public RoomDataView()
		{
			InitializeComponent();
		}

		public RoomDataView(RoomData Room)
		{
			InitializeComponent();
			this.Room = Room;
		}

		private void Rebind()
		{
			DataContext = null;
			DataContext = this;
		}

		public void Clear()
		{
			UserList.ItemsSource = new List<string>();
			Room = null;
		}
	}
}
