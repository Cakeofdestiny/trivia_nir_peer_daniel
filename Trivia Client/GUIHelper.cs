﻿using MaterialDesignThemes.Wpf;
using System.Threading;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Trivia_Client
{
	//A class to help send messages to the server and send data back to the client.
	//Sends the data in a different thread and funnels responses to a delegate function or to the snackbar.
	class GUIHelper
	{
		public delegate void FunctionToExecute();

		public delegate void ProcessError(string resp);

		public static void executeFunction(FunctionToExecute func, ProcessError errorFunction)
		{
			Thread executionThread = new Thread(() => executeThreaded(func, errorFunction));
			executionThread.Start();
		}

		public static void executeFunction(FunctionToExecute func, SnackbarMessageQueue snackbarMQueue)
		{
			executeFunction(func, (string resp) => { snackbarMQueue.Enqueue(resp, true); });
		}

		private static void executeThreaded(FunctionToExecute func, ProcessError errorFunction)
		{
			try
			{
				func();
			}
			catch (TriviaException e)
			{
				errorFunction(e.Message);
			}
		}

		public static int TryParseInt(string text)
		{
			Int32.TryParse(text, out int x);
			return x;
		}
	}
}
