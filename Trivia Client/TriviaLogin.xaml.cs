﻿using MaterialDesignThemes.Wpf;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for TriviaLogin.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly ServerConnection conn = ServerConnection.Instance;
		private readonly SnackbarMessageQueue snackbarMQueue;
		private bool successfullyConnected = false;

		public MainWindow()
		{
			InitializeComponent();
			snackbarMQueue = MainSnackbar.MessageQueue;
			GUIHelper.executeFunction(() => { TryToConnect(Constants.ServerIP, Constants.Port); }
			, ConnectionError);
		}

		private void TryToConnect(string IP, int port)
		{
			snackbarMQueue.Enqueue("Connecting to " + IP + ":" + port.ToString() + "...");
			//Connect and set the variable to true
			conn.Connect(IP, port);
			this.successfullyConnected = true;
			snackbarMQueue.Enqueue("Connected successfully.");
		}

		private void ConnectionError(string resp)
		{
			Dispatcher.BeginInvoke((System.Action)(() => { this.IPPortDialog.IsOpen = true; }));
		}

		private void LoginTextbox_KeyDown(object sender, RoutedEventArgs e)
		{
			LoginSubmitBtn.IsEnabled = successfullyConnected &&
				LoginPasswordTextbox.Password != "" && LoginUsernameTextbox.Text != "";
		}
		private void SignupTextbox_KeyDown(object sender, RoutedEventArgs e)
		{
			SignupSubmitBtn.IsEnabled = successfullyConnected &&
				SignupPasswordTextbox.Password != "" && SignupUsernameTextbox.Text != "";
		}

		private void SetButtonVisibility(bool visibility)
		{
			//Make sure that we successfully connected to the socket
			LoginSubmitBtn.IsEnabled = visibility;
			SignupSubmitBtn.IsEnabled = visibility;
		}

		private void LoginSubmitBtn_Click(object sender, RoutedEventArgs e)
		{
			LoginRequest req = new LoginRequest
			{
				username = LoginUsernameTextbox.Text,
				password = LoginPasswordTextbox.Password
			};

			SetButtonVisibility(false);
			GUIHelper.executeFunction(() => { LoginOp(req); }, MessageToSnackbar);
		}

		private void SignupSubmitBtn_Click(object sender, RoutedEventArgs e)
		{
			SignupRequest req = new SignupRequest
			{
				username = SignupUsernameTextbox.Text,
				email = SignupEmailTextbox.Text,
				password = SignupPasswordTextbox.Password
			};

			SetButtonVisibility(false);
			GUIHelper.executeFunction(() => { SignupOp(req); }, MessageToSnackbar);
		}

		private void SignupOp(SignupRequest req)
		{
			conn.Signup(req);
			Dispatcher.BeginInvoke((System.Action)(() => { MoveToMenu(req.username); }));
		}

		private void LoginOp(LoginRequest req)
		{

			conn.Login(req);
			Dispatcher.BeginInvoke((System.Action)(() => { MoveToMenu(req.username); }));
		}

		private void MoveToMenu(string username)
		{
			TriviaMenu menu = new TriviaMenu(username);
			menu.Show();
			Close();
		}

		private void MessageToSnackbar(string message)
		{
			snackbarMQueue.Enqueue(message, true);
			Dispatcher.BeginInvoke((System.Action)(() =>
			{
				SetButtonVisibility(true);
			}));
		}

		private void PortOnly_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			//UInt16 because a port is 2 unsigned bytes
			e.Handled = !UInt16.TryParse((sender as TextBox).Text + e.Text, out _);
		}

		private void Connect_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.ConnectButton != null)
			{
				this.ConnectButton.IsEnabled = UInt16.TryParse(this.PortTextBox.Text, out _) &&
										  IPAddress.TryParse(this.IPTextBox.Text, out _);
			}
		}

		private void ConnectButton_Click(object sender, RoutedEventArgs e)
		{
			ushort port = 0;
			string IP = this.IPTextBox.Text;
			UInt16.TryParse(PortTextBox.Text, out port);
			GUIHelper.executeFunction(() => { TryToConnect(IP, port); }
			, ConnectionError);
		}
	}
}
