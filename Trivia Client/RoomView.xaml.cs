﻿using MaterialDesignThemes.Wpf;
using System.Windows;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for RoomView.xaml
	/// </summary>
	public partial class RoomView : Window
	{
		private bool runRefresh;
		private readonly bool isAdmin;
		private readonly RoomDataView view;
		private readonly ServerConnection conn = ServerConnection.Instance;
		private readonly SnackbarMessageQueue snackbarMQueue;

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			runRefresh = false;
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			runRefresh = false;
		}

		public RoomView(bool IsAdmin)
		{
			InitializeComponent();
			snackbarMQueue = MainSnackbar.MessageQueue;
			isAdmin = IsAdmin;
			SetControlVisibility();
			runRefresh = true;
			UsernameChip.Content = TriviaMenu.username;
			GUIHelper.executeFunction(RefreshRoomStateOp, snackbarMQueue);
		}

		//To create the roomview for the admin
		public RoomView(CreateRoomRequest req) : this(true)
		{
			RoomData roomData = new RoomData
			{
				maxPlayers = req.maxUsers,
				name = req.roomName,
				questionCount = req.questionCount,
				timePerQuestion = req.answerTimeOut,
			};

			view = new RoomDataView(roomData);
			RoomDataViewFrame.Navigate(view);
			Title = "Room - " + req.roomName;

		}

		public RoomView(RoomData room) : this(false)
		{
			view = new RoomDataView(room);
			RoomDataViewFrame.Navigate(view);
			Title = "Room - " + room.name;
		}

		private void SetControlVisibility()
		{
			AdminStackPanel.Visibility = isAdmin ? Visibility.Visible : Visibility.Collapsed;
			LeaveGameButton.Visibility = isAdmin ? Visibility.Collapsed : Visibility.Visible;
			WaitingPrompt.Visibility = isAdmin ? Visibility.Collapsed : Visibility.Visible;
		}

		private void RefreshRoomStateOp()
		{
			while (runRefresh)
			{
				GetRoomStateResponse resp = conn.GetRoomState();
				if (resp.gameStatus == (int)Constants.GameStatus.GAME_CLOSED)
				{
					LeaveGameOp();
				}
				else if (resp.gameStatus == (int)Constants.GameStatus.GAME_PLAYING)
				{
                    //Not sure if OK
                    StartGameOp();
                }
				Dispatcher.BeginInvoke((System.Action)(() =>
				{
					view.Room.isActive = resp.gameStatus;
					view.Users = resp.players;
				}));
				System.Threading.Thread.Sleep(Constants.DelayMs);

			}
		}



		private void StartGameButton_Click(object sender, RoutedEventArgs e)
		{
            GUIHelper.executeFunction(StartGameOp, snackbarMQueue);
        }

		private void CloseGameButton_Click(object sender, RoutedEventArgs e)
		{
			GUIHelper.executeFunction(CloseGameOp, snackbarMQueue);
		}

		private void LeaveGameButton_Click(object sender, RoutedEventArgs e)
		{
			GUIHelper.executeFunction(LeaveGameOp, snackbarMQueue);
		}

		private void LeaveGameOp()
		{
			Dispatcher.BeginInvoke((System.Action)(() => { LeaveGameButton.IsEnabled = false; }));
			conn.LeaveRoom();
			MoveToMenu();
		}

		private void CloseGameOp()
		{
			Dispatcher.BeginInvoke((System.Action)(() =>
			{
				CloseGameButton.IsEnabled = false;
				StartGameButton.IsEnabled = false;
			}));
			conn.CloseRoom();
			MoveToMenu();
		}

        private void StartGameOp()
        {
            Dispatcher.BeginInvoke((System.Action)(() =>
            {
                StartGameButton.IsEnabled = false;
            }));
            conn.StartRoom();
            MoveToPlay();
        }


        private void MoveToMenu()
		{
			Dispatcher.BeginInvoke((System.Action)(() =>
			{
				TriviaMenu menu = new TriviaMenu();
				menu.Show();
				Close();
			}));
		}

        private void MoveToPlay()
        {
            Dispatcher.BeginInvoke((System.Action)(() =>
            {
                TriviaGame game = new TriviaGame();
                game.Show();
                Close();
            }));
        }
    }
}
