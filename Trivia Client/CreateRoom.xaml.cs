﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for CreateRoom.xaml
	/// </summary>
	public partial class CreateRoom : Page
	{
		private readonly ServerConnection conn = ServerConnection.Instance;

		public CreateRoom()
		{
			InitializeComponent();
		}

		private void RoomTextbox_KeyDown(object sender, RoutedEventArgs e)
		{
			CreateRoomSubmitBtn.IsEnabled = RoomNameTextBox.Text != "" &&
												 NumPlayersTextBox.Text != "" &&
												 NumQuestionsTextBox.Text != "" &&
												 TimePerQuestionTextBox.Text != "";

		}

		private void CreateRoomSubmitBtn_Click(object sender, RoutedEventArgs e)
		{
			CreateRoomRequest req = new CreateRoomRequest
			{
				roomName = RoomNameTextBox.Text,
				maxUsers = GUIHelper.TryParseInt(NumPlayersTextBox.Text),
				questionCount = GUIHelper.TryParseInt(NumQuestionsTextBox.Text),
				answerTimeOut = GUIHelper.TryParseInt(TimePerQuestionTextBox.Text)
			};

			GUIHelper.executeFunction(() => { CreateRoomOp(req); }, TriviaMenu.Snackbar.MessageQueue);
		}

		private void CreateRoomOp(CreateRoomRequest req)
		{
			conn.CreateRoom(req);
			Dispatcher.BeginInvoke((System.Action)(() =>
			{
				RoomView view = new RoomView(req);
				view.Show();
				Window.GetWindow(this).Close();
			}));
		}

		private void NumbersOnly_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = !Int32.TryParse((sender as TextBox).Text + e.Text, out _);
		}
	}
}
