import socket
import json

SERVER_IP = "127.0.0.1"
SERVER_PORT = 1337


def create_sock():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    return sock


def convert_to_json(data):
    json_string = json.dumps(data)
    return json_string


def convert_from_json(json_string):
    data = json.loads(json_string)
    return data


def main():
    sock = create_sock()
    data = "" #Enter data
    json_string = convert_to_json()
    sock.sendall(json_string.encode())
    server_msg = sock.recv(4096)
    server_msg = server_msg.decode()
    sock.close()


if __name__ == "__main__":
    main()