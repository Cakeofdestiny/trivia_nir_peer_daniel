﻿using System;
using System.Windows;
using MaterialDesignThemes.Wpf;
using System.Collections.Generic;

namespace Trivia_Client
{
    /// <summary>
    /// Interaction logic for resultsMenu.xaml
    /// </summary>
    public partial class ResultsMenu :Window
    {
        private readonly SnackbarMessageQueue snackbarMQueue;
        private readonly ServerConnection conn = ServerConnection.Instance;

        public ResultsMenu()
        {
            InitializeComponent();
            GUIHelper.executeFunction(FetchResultsOp, snackbarMQueue);
        }
        private void BackToMenu_click(object sender, RoutedEventArgs e)
        {
            GUIHelper.executeFunction(ReturnMenuOp, snackbarMQueue);
        }

        private void FetchResultsOp()
        {
            GetGameResultsResponse resp = conn.GetResults();
            List<PlayerResults> results = resp.results;
            //Dispatcher.BeginInvoke((System.Action)(() => { HighscoreListbox.ItemsSource = highscores; }));
            //Need to fix xaml file
        }

        private void ReturnMenuOp()
        {
            Dispatcher.BeginInvoke((System.Action)(() => { }));
            MoveToMenu();
        }
        private void MoveToMenu()
        {
            Dispatcher.BeginInvoke((System.Action)(() =>
            {
                TriviaMenu menu = new TriviaMenu();
                menu.Show();
                Close();
            }));
        }
    }

    public class Results
    {
        public Results(PlayerResults results)
        {
            //username = results.username;
            Correct = results.correctAnswerCount;
            Wrong = results.wrongAnswerCount;
            Time = results.averageAnswerTime;
        }
        public int Correct { get; set; }
        public int Wrong { get; set; }
        public int Time { get; set; }
    }
}
