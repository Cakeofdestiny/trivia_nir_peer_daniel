using System.Collections.Generic;

namespace Trivia_Client
{
    public class MenuItem
    {
        public string XAMLPage { get; set; }
        public string Icon { get; set; }
        public string Text { get; set; }
    }

    public class RoomData
    {
        public uint id { get; set; } = 0;
        public string name { get; set; }
        public int maxPlayers { get; set; } //If we ever want to store data as a negative (say, -1 for unlimited)
        public int questionCount { get; set; }
        public int timePerQuestion { get; set; }
        public int isActive { get; set; }
    };

    public struct LoginRequest
    {
        public string username;
        public string password;
    };

    public struct SignupRequest
    {
        public string username;
        public string password;
        public string email;
    };

    public struct GetPlayersInRoomRequest
    {
        public uint roomId;
    };

    public struct JoinRoomRequest
    {
        public uint roomId;
    };

    public struct CreateRoomRequest
    {
        public string roomName;
        public int maxUsers;
        public int questionCount;
        public int answerTimeOut;
    };

    //Responses

    public struct Response
    {
        public int code;
        public byte[] buff;
    }

    public struct GetRoomStateResponse
    {
        public int gameStatus;
        public List<string> players;
        public int questionCount;
        public int answerTimeOut;
    };

    public struct ServerErrorResponse
    {
        public string error;
        public int status;
    };

    public struct UserErrorResponse
    {
        public string error;
        public int status;
    };

    public struct GetRoomsResponse
    {
        public List<RoomData> rooms;
    };

    public struct GetPlayersInRoomResponse
    {
        public List<string> names;
    };

    public struct RawHighscore
    {
        public string user;
        public double score;
    }

    public struct HighscoreResponse
    {
        public List<RawHighscore> highscores;
    };

    public struct SubmitAnswerRequest
    {
        public int answerId;
    };

    public struct GetQuestionResponse
    {
        public int status;
        public string question;
        public SortedDictionary<int, string> answers;
    };

    public struct SubmitAnswerResponse
    {
        public int status;
        public int correctAnswerId;
    };

    public struct GetGameResultsResponse
    {
        public int status;
        public List<PlayerResults> results;
    };

    public struct PlayerResults
    {
        public string username;
        public int correctAnswerCount;
        public int wrongAnswerCount;
        public int averageAnswerTime;
    }
}
