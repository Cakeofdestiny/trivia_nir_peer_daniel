﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for Highscores.xaml
	/// </summary>
	public partial class Highscores : Page
	{
		private readonly ServerConnection conn = ServerConnection.Instance;

		public Highscores()
		{
			InitializeComponent();
			GUIHelper.executeFunction(FetchHighscoresOp, TriviaMenu.Snackbar.MessageQueue);
		}

		private void HighscoreListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//Just a way to unselect any highscores. ListBox provides some nice design features so I'm using it.
			if (null != sender && sender is ListBox)
			{
				(sender as ListBox).SelectedIndex = -1;
			}
		}

		private void FetchHighscoresOp()
		{
			HighscoreResponse resp = conn.GetHighscores();
			List<Highscore> highscores = new List<Highscore>();
			for (int i = 0; i < resp.highscores.Count; i++)
			{
				highscores.Add(new Highscore(resp.highscores[i], i + 1));
			}

			Dispatcher.BeginInvoke((System.Action)(() => { HighscoreListbox.ItemsSource = highscores; }));
		}
	}
	public class Highscore
	{
		public Highscore(RawHighscore highscore, int position)
		{
			Username = highscore.user;
			Score = highscore.score;
			Position = position;
		}

		public string Username { get; set; }
		public int Position
		{
			set
			{
				string[] colors = { "Gold", "Silver", Constants.Bronze };
				switch (value)
				{
					case 1:
					case 2:
					case 3:
						IconColor = colors[value - 1];
						break;

					default:
						//Disable the trophy icon if it's not in the first three positions
						Icon = "";
						break;
				}
			}
		}
		public double Score { get; set; }
		public string IconColor { get; set; }
		public string Icon = "Trophy";
	}
}
