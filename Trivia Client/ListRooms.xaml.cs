﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for ListRooms.xaml
	/// </summary>
	public partial class ListRooms : Page
	{
		private RoomDataView view;
		private readonly ServerConnection conn = ServerConnection.Instance;
		public static bool runRefresh;

		public ListRooms()
		{
			InitializeComponent();
		}

		private void ListRooms_Loaded(object sender, RoutedEventArgs e)
		{
			runRefresh = true;
			GUIHelper.executeFunction(RefreshAllLoopOp, TriviaMenu.Snackbar.MessageQueue);
		}

		private void ListRooms_Unloaded(object sender, RoutedEventArgs e)
		{
			runRefresh = false;
		}

		private void RoomListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			JoinRoomButton.IsEnabled = true;
			view.Room = (RoomData)(sender as ListBox).SelectedItem;
			GUIHelper.executeFunction(FetchRoomUsersOp, TriviaMenu.Snackbar.MessageQueue);
		}

		private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
		{
			if (view.Room != null)
			{
				GUIHelper.executeFunction(() => { JoinRoomOp(view.Room); }, TriviaMenu.Snackbar.MessageQueue);
			}
		}

		private void RoomDataViewFrame_LoadCompleted(object sender, NavigationEventArgs e)
		{
			view = (sender as Frame).Content as RoomDataView;

		}

		private void JoinRoomOp(RoomData roomData)
		{
			conn.JoinRoom(new JoinRoomRequest { roomId = roomData.id });
			Dispatcher.BeginInvoke((System.Action)(() =>
			{
				RoomView view = new RoomView(roomData);
				view.Show();
				//Close the encompassing window
				Window.GetWindow(this).Close();
			}));
		}

		private void RefreshRoomsOp()
		{
			GetRoomsResponse resp = conn.GetRoomList();
			List<RoomData> rooms = resp.rooms;
			if (rooms != null)
			{
				Dispatcher.BeginInvoke((System.Action)(() =>
				{
#pragma warning disable IDE0019 // Use pattern matching
					RoomData currItem = RoomListBox.SelectedItem as RoomData;
#pragma warning restore IDE0019 // Use pattern matching

					RoomListBox.ItemsSource = rooms;
					if (currItem != null)
					{
						int ItemIndex = rooms.FindIndex((RoomData r) => { return r.id == currItem.id; });
						if (ItemIndex != -1)
						{
							//Retain the selection if the room is still available
							RoomListBox.SelectedIndex = ItemIndex;
						}
					}
				}));
			}
			else
			{
				Dispatcher.BeginInvoke((System.Action)(() =>
				{
					RoomListBox.ItemsSource = null;
					JoinRoomButton.IsEnabled = false;
				}));
			}
		}

		private void FetchRoomUsersOp()
		{
			if (view == null)
				return;

			RoomData roomData = null;
			Dispatcher.BeginInvoke((System.Action)(() => { roomData = view.Room; })).Wait();

			if (roomData != null)
			{
				GetPlayersInRoomRequest req = new GetPlayersInRoomRequest { roomId = roomData.id };
				GetPlayersInRoomResponse resp = conn.GetPlayers(req);
				Dispatcher.BeginInvoke((System.Action)(() => { view.Users = resp.names; }));
			}
			else
			{
				Dispatcher.BeginInvoke((System.Action)(() => { view.Clear(); }));
			}
		}

		private void RefreshAllLoopOp()
		{
			while (runRefresh)
			{
				FetchRoomUsersOp();
				RefreshRoomsOp();
				System.Threading.Thread.Sleep(Constants.DelayMs);
			}
		}
	}
}
