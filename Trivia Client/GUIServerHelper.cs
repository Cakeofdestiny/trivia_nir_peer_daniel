﻿using MaterialDesignThemes.Wpf;
using System.Threading;

namespace Trivia_Client
{
	//A class to help send messages to the server and send data back to the client.
	//Sends the data in a different thread and funnels responses to a delegate function or to the snackbar.
	class GUIServerHelper
	{
		public delegate void FunctionToExecute();

		public delegate void ProcessError(ErrorResponse resp);

		public static void executeFunction(FunctionToExecute func, ProcessError errorFunction)
		{
			Thread executionThread = new Thread(() => executeThreaded(func, errorFunction));
			executionThread.Start();
		}

		public static void executeFunction(FunctionToExecute func, SnackbarMessageQueue snackbarMQueue)
		{
			executeFunction(func, (ErrorResponse resp) => { snackbarMQueue.Enqueue(resp.Message, true); });
		}

		private static void executeThreaded(FunctionToExecute func, ProcessError errorFunction)
		{
			try
			{
				func();
			}
			catch (ErrorResponse e)
			{
				errorFunction(e);
			}
		}
	}
}
