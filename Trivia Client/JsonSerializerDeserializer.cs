﻿using Newtonsoft.Json;
using System.Text;

namespace Trivia_Client
{
	public class JsonDeserializer
	{
		//I: A byte buffer containing only the json that represents the wanted message.
		//The caller is responsible for making sure that the response is of the adequate type.
		public static T Deserialize<T>(byte[] buffer)
		{
			string json = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
			T data = JsonConvert.DeserializeObject<T>(json);
			return data;
		}
	}

	class JsonSerializer
	{
		private struct EmptyRequest { };
		private static byte[] CreateBuffer<T>(T requestStruct, Constants.RequestCodes code)
		{
			string json = JsonConvert.SerializeObject(requestStruct, Formatting.None);
			int messageLength = json.Length + Constants.CodeLen;
			string LenString = messageLength.ToString().PadLeft(Constants.SizeLen, '0');
			string CodeString = ((int)code).ToString().PadLeft(Constants.CodeLen, '0');

			return Encoding.UTF8.GetBytes(LenString + CodeString + json);
		}

		private static byte[] CreateBuffer(Constants.RequestCodes code)
		{
			EmptyRequest empty;
			return CreateBuffer(empty, code);
		}

		public static byte[] SerializeLogin(LoginRequest loginRequest)
		{
			return CreateBuffer(loginRequest, Constants.RequestCodes.REQ_LOGIN);
		}

		public static byte[] SerializeSignup(SignupRequest signupRequest)
		{
			return CreateBuffer(signupRequest, Constants.RequestCodes.REQ_SIGNUP);
		}

		public static byte[] LogoutSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_SIGNOUT);
		}

		public static byte[] ViewRoomListSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_ROOM_LIST);
		}

		public static byte[] ViewHighScoresSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_HIGH_SCORES);
		}

		public static byte[] ListPlayersInRoomSerializer(GetPlayersInRoomRequest getPlayersInRoomRequest)
		{
			return CreateBuffer(getPlayersInRoomRequest, Constants.RequestCodes.REQ_LIST_ROOM_PLAYERS);
		}

		public static byte[] CreateRoomSerializer(CreateRoomRequest createRoomRequest)
		{
			return CreateBuffer(createRoomRequest, Constants.RequestCodes.REQ_CREATE_ROOM);
		}

		public static byte[] JoinRoomSerializer(JoinRoomRequest joinRoomRequest)
		{
			return CreateBuffer(joinRoomRequest, Constants.RequestCodes.REQ_JOIN_ROOM);
		}

		public static byte[] CloseRoomSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_CLOSE_ROOM);
		}

		public static byte[] StartRoomSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_START_GAME);
		}

		public static byte[] GetRoomStateSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_GET_STATE);
		}

		public static byte[] LeaveRoomSerializer()
		{
			return CreateBuffer(Constants.RequestCodes.REQ_LEAVE_ROOM);
		}

        public static byte[] SubmitAnswerSerializer(SubmitAnswerRequest answer)
        {
            return CreateBuffer(answer, Constants.RequestCodes.REQ_ANSWER);
        }

        public static byte[] GetQuestionSerializer()
        {
            return CreateBuffer(Constants.RequestCodes.REQ_QUESTION);
        }

        public static byte[] GetResultsSerializer()
        {
            return CreateBuffer(Constants.RequestCodes.REQ_RESULTS);
        }
	}
}
