﻿using MaterialDesignThemes.Wpf;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Trivia_Client
{
	/// <summary>
	/// Interaction logic for TriviaMenu.xaml
	/// </summary>
	public partial class TriviaMenu : Window
	{
		public static Snackbar Snackbar;

		//Default param is for debugging when I wanna open the frontend without going through login
		public static string username { get; private set; }

		public TriviaMenu() : this(username) { }

		public TriviaMenu(string Username)
		{
			InitializeComponent();
			//Bind menu items
			List<MenuItem> items = new List<MenuItem>
			{
				new MenuItem() { Icon = "FormatListBulleted", Text = "List Rooms", XAMLPage = "ListRooms.xaml" },
				new MenuItem() { Icon = "PlaylistPlus", Text = "Create a Room", XAMLPage = "CreateRoom.xaml" },
				new MenuItem() { Icon = "Trophy", Text = "Highscores", XAMLPage = "Highscores.xaml" }
			};

			MenuItemsListBox.ItemsSource = items;

			UsernameChip.Content = Username;
			username = Username;
			Snackbar = MainSnackbar;
		}

		private void Exit_Click(object sender, RoutedEventArgs e)
		{
			//TODO: Call exit function
			System.Windows.Application.Current.Shutdown();
		}

		private void HamburgerMenuClick(object sender, MouseButtonEventArgs e)
		{
			MenuToggleButton.IsChecked = false;
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{ 
			ListRooms.runRefresh = false;
		}
	}
}
